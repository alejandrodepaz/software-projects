# My Projects
This repository serves as a portfolio of personal and class-related projects
which I've completed throughout my academic career at UO. While most of these 
projects were completed for fun in my spare time, a couple are simply projects
I did for classes which I thoroughly enjoyed. Descriptions for each project in 
this repository can be found in each project's respective directory. Any questions 
can be directed to me via email: adepaz@cs.uoregon.edu
