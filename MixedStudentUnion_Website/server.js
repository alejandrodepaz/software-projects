const express = require("express");
const path = require("path");
const app = express();
const MongoClient = require('mongodb').MongoClient
app.use(express.static("frontend"));

//Connect to MongoDB client
MongoClient.connect('mongodb://localhost:27017/animals', function (err, client) {
    if (err){
      throw err;
    } 
})

app.get("/", function(req, res) {
  var full_path = path.join(__dirname, "frontend/html/index.html");
  res.sendFile(full_path);
});

app.get("/about", function(req, res) {
  var full_path = path.join(__dirname, "frontend/html/about.html");
  res.sendFile(full_path);
});

var server = app.listen(5000, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log("MSU Website listening at http://%s:%s", host, port);
});
