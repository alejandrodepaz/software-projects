#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#include "thread_safe_bounded_queue.c"
#include "bounded_queue.c"
#define QUACKSIZE 100
#define CAPTIONSIZE 100

/*
Part 1:

fill queue up
empty queue
fill queue up
empty queue

Part 2:

#DEFINE MAX_THREADS
#DEFINE MAX_TOPICS


struct Publisher * prod_arr[MAX_THREADS];
struct Subscriber * sub_arr[MAX_THREADS];
struct topic_queue * topics[MAX_TOPICS];

all arrays set initialized to null values.
if im gonnna allocate a prod/sub/topicqueue, walk the array, find next NULL value, then malloc item and put it in location (return id)

Part 3:

After end of file being read:

join ALL producers first
join ALL subscribers next
set Exit = 1
join cleanup
*/

struct topic_queue{

    struct thread_safe_bounded_queue *queue;
    pthread_mutex_t lock;
    char *topic_name;
    int topicNum;

};

struct topic_queue* MallocTopicQueue(int size){
    
    struct topic_queue *returnValue = NULL;
    returnValue = (struct topic_queue*)malloc(sizeof(struct topic_queue));
    pthread_mutex_init(&(returnValue->lock), NULL);
    returnValue->queue = (struct thread_safe_bounded_queue *)TS_BB_MallocBoundedQueue(size);

    return returnValue;
}

long long Enqueue(struct topic_queue *t_queue, struct TopicEntry *entry){

    struct thread_safe_bounded_queue *queue = t_queue->queue;
    long long returnValue = 0;

    pthread_mutex_lock(&(t_queue->lock));
    int id = TS_BB_TryEnqueue(queue, entry);
    if (id >= 0) {
        entry->entrynum = id;
        returnValue = 1;
    }
    pthread_mutex_unlock(&(t_queue->lock));

    return returnValue;
}

int Dequeue(struct topic_queue *t_queue, long long id){

    struct thread_safe_bounded_queue *queue = t_queue->queue;

    int returnValue = 0;
    pthread_mutex_lock(&(t_queue->lock));
    returnValue = TS_BB_TryDequeue(queue, id);
    pthread_mutex_unlock(&(t_queue->lock));

    return returnValue;

}


long long GetEntry(struct topic_queue *t_queue, long long id, struct TopicEntry **entry){

    struct thread_safe_bounded_queue *queue = t_queue->queue;

    long long val = 0;
    pthread_mutex_lock(&(t_queue->lock));

    if (TS_BB_IsEmpty(queue)){
        val = -1;
    } else if (TS_BB_IsIdValid(queue, id)){
        
        (*entry) = TS_BB_GetItem(queue, id);
        val = id + 1;

    } else if (id < queue->queue->tail){
        
        (*entry) = TS_BB_GetItem(queue, queue->queue->tail);
        val = queue->queue->tail + 1;

    } else if (id > queue->queue->head){
        
        (*entry) = TS_BB_GetItem(queue, queue->queue->head - 1);
        val = -1;
    }

    pthread_mutex_unlock(&(t_queue->lock));

    return val;
}

int IsEmpty(struct topic_queue *t_queue){

    struct thread_safe_bounded_queue *queue = t_queue->queue;
    int val = 0;
    pthread_mutex_lock(&(t_queue->lock));
    val = TS_BB_IsEmpty(queue);
    pthread_mutex_unlock(&(t_queue->lock));
    return val;

}

int IsFull(struct topic_queue *t_queue){

    struct thread_safe_bounded_queue *queue = t_queue->queue;
    int val = 0;
    pthread_mutex_lock(&(t_queue->lock));
    val = TS_BB_IsFull(queue);
    pthread_mutex_unlock(&(t_queue->lock));

    return val;
}

void FreeQueue(struct topic_queue *t_queue){

    struct thread_safe_bounded_queue *queue = t_queue->queue;

    pthread_mutex_lock(&(t_queue->lock));
    TS_BB_FreeBoundedQueue(queue);
    pthread_mutex_unlock(&(t_queue->lock));


}

struct TopicEntry* MallocTopicEntry(){

    struct TopicEntry *returnValue = NULL;
    returnValue = (struct TopicEntry*)malloc(sizeof(struct TopicEntry));
    
    return returnValue;
}