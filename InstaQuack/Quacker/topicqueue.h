#ifndef TQ_H
#define TQ_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#define QUACKSIZE 100
#define CAPTIONSIZE 100

struct TopicEntry {

    int entrynum;
    struct timeval timestamp;
    int pubID;
    char photoUrl[QUACKSIZE]; /* URL to photo  */
    char photoCaption[CAPTIONSIZE];  /* photo caption */
};

typedef struct topic_queue TopicQueue;

struct topic_queue* MallocTopicQueue(int size);

long long GetEntry(TopicQueue *t_queue, long long id, struct TopicEntry **entry);

long long Enqueue(TopicQueue *t_queue, struct TopicEntry *entry);

int Dequeue(TopicQueue *t_queue, long long id);

int IsFull(TopicQueue *t_queue);

int IsEmpty(TopicQueue *t_queue);

void FreeQueue(TopicQueue *t_queue);

struct TopicEntry* MallocTopicEntry();

#endif