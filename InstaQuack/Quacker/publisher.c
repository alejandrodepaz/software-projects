#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#define MAXTOPICS 100

struct Publisher{

    pthread_t *TID;
    int pubID;
    char *filename;
    struct topic_queue **topic_queues;
};

struct Publisher* MallocPublisher(pthread_t *tid, int id, char *f_name){

    struct Publisher *pub = NULL;
    pub = (struct Publisher*)malloc(sizeof(struct Publisher));
    pub->TID = tid;
    pub->pubID = id;
    pub->filename = strdup(f_name);
    pub->topic_queues = (struct topic_queue **)malloc(MAXTOPICS*sizeof(struct topic_queue *));
    
    return pub;
}