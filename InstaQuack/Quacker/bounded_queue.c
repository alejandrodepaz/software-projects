#include <stdio.h>
#include <stdlib.h>
#include "bounded_queue.h"

struct bounded_queue
{
        int size;       // capacity
        void **buffer;  // storage
        long long head; // 
        long long tail; //
};

int RoundIDToBufferIndex(int size, long long index)
{
        long long value = (index % ((long long)size));
        return (int)value;
}

BoundedQueue *BB_MallocBoundedQueue(long size)
{
        struct bounded_queue *returnValue = malloc(sizeof(struct bounded_queue));
        returnValue->size = size;
        returnValue->buffer = (void **) malloc(size*sizeof(void *));

        int i;
        for(i=0; i<size; i++){
                returnValue->buffer[i] = NULL;
        }

        returnValue->head = (long long) returnValue->buffer[0];
        returnValue->tail = (long long) returnValue->buffer[0];

        return (BoundedQueue *)returnValue;
}

long long BB_TryEnqueue(struct bounded_queue *queue,void *item)
{       
        long long returnValue = -1;

        if (!BB_IsFull(queue)){
  
                int val = RoundIDToBufferIndex(queue->size, queue->head);
                queue->buffer[val] = item;
                queue->head += 1;
                
                returnValue = queue->head-1;
        }
        return returnValue;
}

int BB_TryDequeue(struct bounded_queue *queue,long long id)
{
        int returnValue = 0;
        //printf("Empty?: %d  Valid ID: %d\n", BB_IsEmpty(queue), BB_IsIdValid(queue, id));
        //printf("QUEUE TAIL: %lld  ID: %lld\n", queue->tail, id);
        if (!BB_IsEmpty(queue) && BB_IsIdValid(queue, id) && queue->tail == id){
                
                int val = RoundIDToBufferIndex(queue->size, queue->tail);
                queue->buffer[val] = NULL;
                queue->tail += 1;

                returnValue = 1;
        }
        return returnValue;
}

long long BB_GetFront(struct bounded_queue *queue)
{
        long long returnValue = -1;
        if(!BB_IsEmpty(queue))
        {
                returnValue = queue->head-1;
        }
        return returnValue;
}

long long BB_GetBack(struct bounded_queue *queue)
{
        long long returnValue = -1;
        if(!BB_IsEmpty(queue))
        {
                returnValue = queue->tail;
        }
        return returnValue;
}

int BB_GetCount(struct bounded_queue *queue)
{
        long long returnValue = 0;
        if (!BB_IsEmpty(queue)){
                
                returnValue = queue->head - queue->tail;
        }
        return (int)returnValue;
}

int BB_IsIdValid(struct bounded_queue *queue,long long id)
{
        int returnValue = 0;

        if (queue->tail <= id && queue->head > id){
                returnValue = 1;
        }

        return returnValue;
}

void *BB_GetItem(struct bounded_queue *queue,long long id)
{
        void *returnValue = NULL;

        if (BB_IsIdValid(queue, id)){

                int val = RoundIDToBufferIndex(queue->size, id);
                returnValue = queue->buffer[val];
        }
        return returnValue;
}

int BB_IsFull(struct bounded_queue *queue)
{
        int returnValue = 0;
        if (queue->head - queue->tail == queue->size){
                returnValue = 1;
        }
        return returnValue;
}

int BB_IsEmpty(struct bounded_queue *queue)
{
        int returnValue = 0;
        if (queue->head == queue->tail){
                returnValue = 1;
        }
        return returnValue;
}

void BB_FreeBoundedQueue(struct bounded_queue *queue)
{
        int i;
        int size = queue->size;
        
        for(i=0; i<size; i++){
                free(queue->buffer[i]);
        }
        free(queue->buffer);
        free(queue);

}

