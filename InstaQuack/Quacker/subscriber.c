#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"

#define MAXTOPICS 100

struct Subscriber{

    pthread_t *TID;
    FILE *file;
    char *filename;
    int subID;
    // stores the last read entry id from each respective queue (e.g. index 0 corresponds to the queue in topic_queues[0],
    // and index 0 holds the id of the last read topic from this queue).
    long long topic_counters[MAXTOPICS]; 
    FILE *topic_files[MAXTOPICS];
    struct topic_queue *topic_queues[MAXTOPICS];

    int last_file;
};

struct Subscriber* MallocSubscriber(pthread_t *TID, char *filename, int id){

    struct Subscriber *sub = NULL;
    sub = (struct Subscriber*)malloc(sizeof(struct Subscriber));
    sub->TID = TID;
    sub->filename = strdup(filename);
    sub->subID = id;
    //sub->topic_counters = (long long *)malloc(MAXTOPICS*sizeof(long long));
    int i;
    for(i=0; i<MAXTOPICS; i++){
        sub->topic_counters[i] = 0;
        sub->topic_files[i] = NULL;
        sub->topic_queues[i] = NULL;
    }
    //sub->topic_files = (FILE **)malloc(MAXTOPICS*sizeof(FILE *));
    //sub->topic_queues = (struct topic_queue **)malloc(MAXTOPICS*sizeof(struct topic_queue *));

    sub->last_file = 0;
    
    return sub;
}