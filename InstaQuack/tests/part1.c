#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#include "topicqueue.c"

int main(){

    struct topic_queue *queue = MallocTopicQueue(5);
    printf("Empty? %d\n", IsEmpty(queue));
    struct timeval tv;
    gettimeofday(&tv, NULL);

    struct TopicEntry newPost = {1, tv, 2, "ABC", "XYZ"};

    printf("\nEnqueue Successful? %lld\n", Enqueue(queue, &newPost));
    printf("Post ID: %d\n", newPost.entrynum);
    printf("Time Stamp: %ld\n", newPost.timestamp.tv_sec);
    printf("Photo URL: %s\n", newPost.photoUrl);
    printf("Photo Caption: %s\n", newPost.photoCaption);
    
    struct timeval tv2;
    gettimeofday(&tv2, NULL);
    struct TopicEntry newPost2 = {1, tv, 2, "ABC", "XYZ"};
    printf("\nEnqueue Successful? %lld\n", Enqueue(queue, &newPost2));
    printf("Post ID: %d\n", newPost2.entrynum);
    printf("Time Stamp: %ld\n", newPost2.timestamp.tv_sec);
    printf("Photo URL: %s\n", newPost2.photoUrl);
    printf("Photo Caption: %s\n", newPost2.photoCaption);

    
    printf("Empty? %d\n", IsEmpty(queue));
    printf("Full? %d\n", IsFull(queue));
    printf("Filling Queue\n");

    int i;
    for(i=0; i<3; i++){
        struct TopicEntry post = {1, tv, 2, "ABC", "XYZ"};
        printf("\nEnqueue Successful? %lld\n", Enqueue(queue, &post));
        printf("Post ID: %d\n", post.entrynum);
        printf("Time Stamp: %ld\n", post.timestamp.tv_sec);
        printf("Photo URL: %s\n", post.photoUrl);
        printf("Photo Caption: %s\n", post.photoCaption);
    }

    printf("\nQueue Full? %d\n", IsFull(queue));
    struct TopicEntry post = {1, tv, 2, "ABC", "XYZ"};
    printf("Enqueue Successful? %lld\n", Enqueue(queue, &post));

    for(i=0; i<5; i++){
        printf("Dequeue Successful? %d\n", Dequeue(queue,i));
    }
    printf("Queue Empty? %d\n", IsEmpty(queue));
    
    struct TopicEntry *post2 = MallocTopicEntry();
    post2->entrynum = 0;
    post2->timestamp = tv;
    post2->pubID = 23;
    strcpy(post2->photoUrl, "www.something.com");
    strcpy(post2->photoCaption, "waddup");
    printf("\nEnqueue Successful? %lld\n", Enqueue(queue, post2));
    printf("Post ID: %d\n", post2->entrynum);
    printf("Time Stamp: %ld\n", post2->timestamp.tv_sec);
    printf("Photo URL: %s\n", post2->photoUrl);
    printf("Photo Caption: %s\n", post2->photoCaption);

    struct TopicEntry *getEntry = MallocTopicEntry();
    //long long last_read = GetEntry(queue, 4, &getEntry);
    printf("\nAttempting to Get Entry with ID=5\n");
    long long id = 5;
    if (GetEntry(queue, id, &getEntry) == id+1){
        printf("Entry query successful\n");
        printf("Entry ID: %d\n", getEntry->entrynum);
        printf("Time Stamp: %ld\n", getEntry->timestamp.tv_sec);
        printf("Photo URL: %s\n", getEntry->photoUrl);
        printf("Photo Caption: %s\n", getEntry->photoCaption);
    } else{
        printf("Entry Query Failed\n");
    }

    printf("\nAttempting to Get Entry with ID=999\n");
    long long id2 = 999;
    if (GetEntry(queue, id2, &getEntry) > 0){
        printf("Entry query successful\n");
        printf("Entry ID: %d\n", getEntry->entrynum);
        printf("Time Stamp: %ld\n", getEntry->timestamp.tv_sec);
        printf("Photo URL: %s\n", getEntry->photoUrl);
        printf("Photo Caption: %s\n", getEntry->photoCaption);
    } else{
        printf("Entry Query Failed\n");
    }


    FreeQueue(queue);

    return 0;
}

