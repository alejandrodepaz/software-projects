#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#include "publisher.c"
#include "subscriber.c"
#include "topicqueue.c"
#define MAX_THREADS 100
#define MAX_TOPICS 100

int Exit = 0;
int Start = 0;

struct Publisher* pub_arr[MAX_THREADS];
struct Subscriber* sub_arr[MAX_THREADS];
struct topic_queue* topics[MAX_TOPICS];

struct TopicEntry *entry;
/*
Publisher func is passed the publisher struct, which has a threadID, and file. It then reads through the file and creates topic entry structs,
which get enqueued.

Subscriber func is passed the subscriber struct, which has a threadID, and file. It then reads through the file and calls GetEntry.

Cleanup 
*/
void *Publisher(void *arg){

    while(Start == 0){
        usleep(1);
    }
    struct Publisher *pub;
    pub = arg;
    printf("Publisher %d Thread Entered\n", pub->pubID);
    printf("Publisher %d Filename: %s\n", pub->pubID, pub->filename);

   return NULL;
}

void *Subscriber(void *arg){

    while(Start == 0){
        usleep(1);
    }

    struct Subscriber *sub;
    sub = arg;
    printf("Subscriber %d Thread Entered\n", sub->subID);
    printf("Subscriber %d Filename: %s\n", sub->subID, sub->filename);

   return NULL;
}

void *Cleanup(void *arg){

    while(Exit == 0){
        usleep(1);
    }

    struct topic_queue *queue = arg;
    /*
    struct TopicEntry *entry = MallocTopicEntry();
    entry = arg;
    long long id = entry->entrynum;
    printf("\nEntering Cleanup\n");
    while(!IsEmpty(queue)){
        printf("Dequeue Successful? %d\n", Dequeue(queue, queue->queue->queue->tail));
    }

    printf("Queue Empty? %d\n", IsEmpty(queue));
    FreeQueue(queue);
    */
    printf("\n");
    printf("Cleanup Thread Entered\n");
    printf("Topic Queue Topic Name: %s\n", queue->topic_name);
    printf("Cleanup Performed, Thread Exited\n");
    return NULL;
}

int main(){

    int i;
    for(i=0; i<MAX_TOPICS; i++){

        topics[i] = NULL;
        pub_arr[i] = NULL;
        sub_arr[i] = NULL;
    }
    struct topic_queue *queue;
    queue = MallocTopicQueue(2);
    char *topic = "CIS 415 Topics";
    queue->topic_name = topic;
    entry = MallocTopicEntry();

    topics[0] = queue;

    pthread_t pub;
    pthread_t pub2;
    pthread_t sub;
    pthread_t sub2;
    pthread_t cleanup;

    char *pub_fname1 = "pub_filename1.txt";
    char *pub_fname2 = "pub_filename2.txt";
    struct Publisher *pubr = MallocPublisher(&pub, 0, pub_fname1);
    struct Publisher *pubr2 = MallocPublisher(&pub2, 1, pub_fname2);

    char *sub_fname1 = "sub_filename1.txt";
    char *sub_fname2 = "sub_filename2.txt";
    struct Subscriber *subr = MallocSubscriber(&sub, sub_fname1, 0);
    struct Subscriber *subr2 = MallocSubscriber(&sub2, sub_fname2, 1);

    if (pthread_create(&pub, NULL, &Publisher, pubr)){
        printf("Failed to Create Thread\n");
        return -1;
    }

    if (pthread_create(&pub2, NULL, &Publisher, pubr2)){
        printf("Failed to Create Thread\n");
        return -1;
    }

    if (pthread_create(&sub, NULL, &Subscriber, subr)){
        printf("Failed to Create Thread\n");
        return -1;
    }

    if (pthread_create(&sub2, NULL, &Subscriber, subr2)){
        printf("Failed to Create Thread\n");
        return -1;
    }

    if (pthread_create(&cleanup, NULL, &Cleanup, queue)){
        printf("Failed to Create Thread\n");
        return -1;

    }

    Start = 1;
    pthread_join(pub, NULL);
    pthread_join(pub2, NULL);
    pthread_join(sub, NULL);
    pthread_join(sub2, NULL);

    Exit = 1;
    pthread_join(cleanup, NULL);

    return 0;
}