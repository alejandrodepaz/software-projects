#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#include "publisher.c"
#include "subscriber.c"
#include "topicqueue.c"
#define MAX_THREADS 100
#define MAX_TOPICS 100

struct Publisher* pub_arr[MAX_THREADS];
struct Subscriber* sub_arr[MAX_THREADS];
struct topic_queue* topics[MAX_TOPICS];
//pthread_t threads[MAX_THREADS];

int threadCount = 0;
int subCount = 0;
int pubCount = 0;
int topicsCount = 0;
int delta;

int Exit = 0;
int Start = 0;

pthread_t cleanup;

int fileLength(char *file){

    FILE *fileptr = fopen(file, "r");
   //extract character from file and store in chr
    char c;
    int num_lines = 0;
    for (c = getc(fileptr); c != EOF; c = getc(fileptr)){
        if (c == '\n'){ // Increment count if this character is newline 
            num_lines += 1;
        }
    }
    // Close the file 
    fclose(fileptr);
    return  num_lines;
}

void *Publisher(void *arg){

    while(Start != 1){
        usleep(1);
    }
    struct Publisher *pub;
    pub = arg;
    int length = strlen(pub->filename);
    char filename[length];
    int i,j;

    for (i=0; i<length-1; i++){ //strip the filename of its quotation marks, so that fopen() works properly.
        filename[i] = pub->filename[i+1];
    }

    filename[length-2] = '\0'; 

    int f_length = fileLength(filename);
    FILE *in;
    in = fopen(filename, "r");

    char *line = NULL;
    ssize_t nread;
    size_t len = 0;

    printf("Publisher thread %d reading from %s\n",pub->pubID, pub->filename);
    int entryCount = 0;
    //printf("FLENGTH PUB: %d\n", f_length);

    for (j=0; j<f_length/4; j++){

        char **commands = (char **)malloc(3*sizeof(char*));

        struct TopicEntry *topic = MallocTopicEntry(); //create topic entry structure.
        for (i=0; i<4; i++){
            
            nread = getline(&line, &len, in);
            line[nread-1] = '\0';
            commands[i] = strdup(line);

        }
        //initialize the TopicEntry struct's values based on the lines read from the given file
        topic->entrynum = entryCount;
        entryCount++;
        struct timeval tv;
        gettimeofday(&tv, NULL);
        topic->timestamp = tv;
        topic->pubID = pub->pubID;
        strcpy(topic->photoUrl, commands[1]);
        strcpy(topic->photoCaption, commands[2]);

        int topic_num = atoi(commands[0]);
        int sleep_time = atoi(commands[3]);
        
        struct topic_queue *queue = topics[topic_num-1];
        while(Enqueue(queue, topic) == 0){
            pthread_yield();
        }
        sleep(sleep_time);
    }

    return NULL;

}

void *Subscriber(void *arg){

    while(Start != 1){
        usleep(1);
    }

    struct Subscriber *sub;
    sub = arg; //assign subscriber struct 

    int length = strlen(sub->filename);
    char filename[length];
    int i,j;

    for (i=0; i<length-1; i++){ //strip the filename of its quotation marks, so that fopen() works properly.
        filename[i] = sub->filename[i+1];
    }

    filename[length-2] = '\0'; 

    int f_length = fileLength(filename);
    FILE *in;
    in = fopen(filename, "r");
    

    char *line = NULL;
    ssize_t nread;
    size_t len = 0;

    printf("Subscriber thread %d reading from %s\n",sub->subID, sub->filename);
    //printf("FLENGTH SUB: %d\n", f_length);
    for (j=0; j<(f_length)/2; j++){

        char **commands = (char **)malloc(3*sizeof(char*));

        for (i=0; i<2; i++){
            
            nread = getline(&line, &len, in);
            line[nread-1] = '\0';
            commands[i] = strdup(line);

        }

        int topic_num = atoi(commands[0]);
        int sleep_time = atoi(commands[1]);
        
        struct topic_queue *queue = topics[topic_num-1];
        long long last_read = sub->topic_counters[topic_num-1];
        
        struct TopicEntry *entry = MallocTopicEntry(); //create empty TopicEntry structure.
        //printf("Last READ: %lld\n", last_read);
        int tryCount = 0;
        while(GetEntry(queue, last_read, &entry) == -1){
            tryCount += 1;
            if (tryCount == 15){
                printf("Subscriber %d Attempted to Access an entry that doesn't exist too many times. Attempt Canceled.\n", sub->subID);
                break;
            }
            pthread_yield();
        }
        if (tryCount != 15){
            char sub_file[50];
            sprintf(sub_file, "sub_%d_entries_%d.txt", sub->subID, topic_num-1);

            FILE *f = fopen(sub_file, "a");

            char entrynum[MAXTOPICS];
            sprintf(entrynum, "Entry Number: %d", entry->entrynum);

            fprintf(f, entrynum);
            fprintf(f, "\n");        
            fprintf(f, entry->photoUrl);
            fprintf(f, "\n");
            fprintf(f, entry->photoCaption);
            fprintf(f, "\n");

            sub->topic_files[sub->last_file] = f;
            sub->topic_counters[topic_num-1] = last_read + 1;
            sub->last_file += 1;
        }
        sleep(sleep_time);
    }
    return NULL;

}

void *Cleanup(void *arg){

    while(Exit != 1){
        int i;
        for(i =0; i<topicsCount; i++){
            struct topic_queue *currQueue = topics[i];
            if(!IsEmpty(currQueue)){

                long long iterator = currQueue->queue->queue->tail;
                struct TopicEntry *entry = MallocTopicEntry();
                while(iterator != currQueue->queue->queue->head){
                    long long val = GetEntry(currQueue, iterator, &entry);
                    if(val != -1){
                        struct timeval now;
                        gettimeofday(&now, NULL);
                        int age = now.tv_sec - entry->timestamp.tv_sec;
                        if (age > delta){
                            printf("Entry %d in Queue %s has Expired. Dequeueing Now.\n", entry->entrynum, currQueue->topic_name);
                            Dequeue(currQueue, iterator);
                        }
                    }else{
                        continue;
                    }
                    iterator++;
                }
            }
        }

    }
    int i;
    for(i=0; i<topicsCount; i++){
        struct topic_queue *queue = topics[i];
        printf("Dequeueing Items from Queue: %s\n", queue->topic_name);
        while(!IsEmpty(queue)){
            Dequeue(queue, queue->queue->queue->tail);
        }
        printf("Queue %s Empty? %d\n", queue->topic_name, IsEmpty(queue));
        FreeQueue(queue);
    }

    printf("Cleanup Performed\n");

    return NULL;
}

void CommandArguments(char *filename){

    FILE *in;
    in = fopen(filename, "r");

    char *line = NULL;
    ssize_t nread;
    size_t len = 0;

    while((nread = getline(&line, &len, in)) != -1){

        const char s[2] = " "; //Delimiter is a space
        char *token; //the string where we will store each word
        line[nread-1] = '\0';

        token = strtok(line, s);
        int length = strlen(token);
        token[length] = '\0';
        
        int counter = 0;
        char *commands[MAXTOPICS];
        commands[0] = token;
        counter = 1;

        while( token != NULL ) { 
            
            token = strtok(NULL, s);
            if (token != NULL){

                commands[counter] = token; //add the current command to the current PCB's command array
                counter += 1;
            }
        }

        //If line reads 'create topic', create a new topic queue and add to global topic queue array
        if(strcmp(commands[0], "create") == 0 && strcmp(commands[1], "topic") == 0){
            
            int topic_num = atoi(commands[2]);
            char *topic_name = (char*)malloc(100*sizeof(char));

            if (counter > 5){

                int i = 3;
                while(1){
                    int str = strlen(commands[i]);
                    if (commands[i][str-1] != '\"'){

                        strcat(topic_name, commands[i]);
                        strcat(topic_name, " ");
                        i++;

                    } else{

                        break;
                    }
                }
                strcat(topic_name, commands[i]);

            } else{

                topic_name = commands[3];
            }
            
            int length = atoi(commands[counter-1]);

            struct topic_queue *queue = MallocTopicQueue(length);
            queue->topic_name = strdup(topic_name);
            queue->topicNum = topic_num;

            topics[topicsCount] = queue;
            topicsCount += 1;

        }

        if(strcmp(commands[0], "create") == 0){ //If line reads 'add', check if its 'subscriber' or 'publisher' we want to add

            if (strcmp(commands[1], "publisher") == 0){ //If pub, create publisher struct and add it to global publisher array

                char *f_name = commands[2];
                pthread_t pubt;
                //is the ID just the order it came in? Yup just use global counter
                struct Publisher *pub = MallocPublisher(&pubt, pubCount, f_name);
                pub_arr[pubCount] = pub;
                pubCount += 1;

                if (pthread_create(&pubt, NULL, &Publisher, pub)){
                    printf("Failed to Create Publisher Thread\n");
                }
                //threads[threadCount] = pubt;
                threadCount += 1; 

            } else if (strcmp(commands[1], "subscriber") == 0){ //If sub, create subscriber struct and add it to global publisher array
                
                char *f_name = commands[2];
                pthread_t subt;
                //is the ID just the order it came in? Yup just use global counter
                struct Subscriber *sub = MallocSubscriber(&subt, f_name, subCount);
                sub_arr[subCount] = sub;
                subCount += 1;

                if (pthread_create(&subt, NULL, &Subscriber, sub)){
                    printf("Failed to Create Publisher Thread\n");
                }       
                //threads[threadCount] = subt;
                threadCount += 1;         
            }
        }

        if(strcmp(commands[0], "query") == 0){ //if line is query, check if we want to query publishers,subscribers, or topic queues.

            if (strcmp(commands[1], "publishers") == 0){ 

                int i;
                for (i=0; i<pubCount; i++){

                    struct Publisher *pub = pub_arr[i];
                    printf("Publisher Thread %d %s\n", pub->pubID, pub->filename);
                }

            } else if (strcmp(commands[1], "subscribers") == 0){ 

                int i;
                for (i=0; i<subCount; i++){

                    struct Subscriber *sub = sub_arr[i];
                    printf("Subscriber Thread %d %s\n", sub->subID, sub->filename);
                }

            } else if (strcmp(commands[1], "topics") == 0){
                
                int i;
                for (i=0; i<topicsCount; i++){

                    struct topic_queue *queue = topics[i];
                    printf("Topic %d %s\n", queue->topicNum, queue->topic_name);
                }
            }
        }

        if(strcmp(commands[0], "Delta") == 0){

            delta = atoi(commands[1]);

            if (pthread_create(&cleanup, NULL, &Cleanup, topics)){
                printf("Failed to Create Cleanup Thread\n");
            }
        
            
        }

        if(strcmp(commands[0], "start") == 0){
            
            Start = 1;
            
            int i;

            for(i=0; i<pubCount; i++){
                struct Publisher *pub = pub_arr[i];
                pthread_join(*pub->TID, NULL);
            }

            for(i=0; i<subCount; i++){
                struct Subscriber *sub = sub_arr[i];
                pthread_join(*sub->TID, NULL);
            }

            Exit = 1;
            pthread_join(cleanup, NULL);
            fclose(in);
        }
    }

}


int main(int argc, char *argv[]){

    int i;
    for(i=0; i<MAX_TOPICS; i++){

        topics[i] = NULL;
    }

    for(i=0; i<MAX_THREADS; i++){
        
        pub_arr[i] = NULL;
        sub_arr[i] = NULL;
        
    }

    CommandArguments(argv[1]);

    return 0;
}