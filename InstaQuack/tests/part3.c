#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include "bounded_queue.h"
#include "thread_safe_bounded_queue.h"
#include "topicqueue.h"
#include "publisher.c"
#include "subscriber.c"
#include "topicqueue.c"
#define MAX_THREADS 100
#define MAX_TOPICS 100

struct Publisher* pub_arr[MAX_THREADS];
struct Subscriber* sub_arr[MAX_THREADS];
struct topic_queue* topics[MAX_TOPICS];
//pthread_t threads[MAX_THREADS];

int threadCount = 0;
int subCount = 0;
int pubCount = 0;
int topicsCount = 0;
int delta;

int Exit = 0;
int Start = 0;

pthread_t cleanup;

void *Publisher(void *arg){

    while(Start != 1){
        usleep(1);
    }
    struct Publisher *pub;
    pub = arg;
    printf("Publisher thread %d reading from %s\n",pub->pubID, pub->filename);

    return NULL;

}

void *Subscriber(void *arg){

    while(Start != 1){
        usleep(1);
    }

    struct Subscriber *sub;
    sub = arg;
    printf("Subscriber thread %d reading from %s\n",sub->subID, sub->filename);

    return NULL;

}

void *Cleanup(void *arg){

    while(Exit != 1){
        usleep(1);
    }

    printf("Cleanup Performed\n");

    return NULL;
}

void CommandArguments(char *filename){

    FILE *in;
    in = fopen(filename, "r");

    char *line = NULL;
    ssize_t nread;
    size_t len = 0;

    while((nread = getline(&line, &len, in)) != -1){

        const char s[2] = " "; //Delimiter is a space
        char *token; //the string where we will store each word
        line[nread-1] = '\0';

        token = strtok(line, s);
        int length = strlen(token);
        token[length] = '\0';
        
        int counter = 0;
        char *commands[MAXTOPICS];
        commands[0] = token;
        counter = 1;

        while( token != NULL ) { 
            
            token = strtok(NULL, s);
            if (token != NULL){

                commands[counter] = token; //add the current command to the current PCB's command array
                counter += 1;
            }
        }

        //If line reads 'create topic', create a new topic queue and add to global topic queue array
        if(strcmp(commands[0], "create") == 0 && strcmp(commands[1], "topic") == 0){
            
            int topic_num = atoi(commands[2]);
            char *topic_name = (char*)malloc(100*sizeof(char));

            if (counter > 5){

                int i = 3;
                while(1){
                    int str = strlen(commands[i]);
                    if (commands[i][str-1] != '\"'){

                        strcat(topic_name, commands[i]);
                        strcat(topic_name, " ");
                        i++;

                    } else{

                        break;
                    }
                }
                strcat(topic_name, commands[i]);

            } else{

                topic_name = commands[3];
            }
            
            int length = atoi(commands[counter-1]);

            struct topic_queue *queue = MallocTopicQueue(length);
            queue->topic_name = strdup(topic_name);
            queue->topicNum = topic_num;

            topics[topicsCount] = queue;
            topicsCount += 1;

        }

        if(strcmp(commands[0], "create") == 0){ //If line reads 'add', check if its 'subscriber' or 'publisher' we want to add

            if (strcmp(commands[1], "publisher") == 0){ //If pub, create publisher struct and add it to global publisher array

                char *f_name = commands[2];
                pthread_t pubt;
                //is the ID just the order it came in? Yup just use global counter
                struct Publisher *pub = MallocPublisher(&pubt, pubCount, f_name);
                pub_arr[pubCount] = pub;
                pubCount += 1;

                if (pthread_create(&pubt, NULL, &Publisher, pub)){
                    printf("Failed to Create Publisher Thread\n");
                }
                //threads[threadCount] = pubt;
                threadCount += 1; 

            } else if (strcmp(commands[1], "subscriber") == 0){ //If sub, create subscriber struct and add it to global publisher array
                
                char *f_name = commands[2];
                pthread_t subt;
                //is the ID just the order it came in? Yup just use global counter
                struct Subscriber *sub = MallocSubscriber(&subt, f_name, subCount);
                sub_arr[subCount] = sub;
                subCount += 1;

                if (pthread_create(&subt, NULL, &Subscriber, sub)){
                    printf("Failed to Create Publisher Thread\n");
                }       
                //threads[threadCount] = subt;
                threadCount += 1;         
            }
        }

        if(strcmp(commands[0], "query") == 0){ //if line is query, check if we want to query publishers,subscribers, or topic queues.

            if (strcmp(commands[1], "publishers") == 0){ 

                int i;
                for (i=0; i<pubCount; i++){

                    struct Publisher *pub = pub_arr[i];
                    printf("Publisher Thread %d %s\n", pub->pubID, pub->filename);
                }

            } else if (strcmp(commands[1], "subscribers") == 0){ 

                int i;
                for (i=0; i<subCount; i++){

                    struct Subscriber *sub = sub_arr[i];
                    printf("Subscriber Thread %d %s\n", sub->subID, sub->filename);
                }

            } else if (strcmp(commands[1], "topics") == 0){
                
                int i;
                for (i=0; i<topicsCount; i++){

                    struct topic_queue *queue = topics[i];
                    printf("Topic %d %s\n", queue->topicNum, queue->topic_name);
                }
            }
        }

        if(strcmp(commands[0], "Delta") == 0){

            delta = atoi(commands[1]);

            if (pthread_create(&cleanup, NULL, &Cleanup, topics)){
                printf("Failed to Create Cleanup Thread\n");
            }
        
            
        }

        if(strcmp(commands[0], "start") == 0){
            
            Start = 1;
            
            int i;
            for(i=0; i<pubCount; i++){
                struct Publisher *pub = pub_arr[i];
                pthread_join(*pub->TID, NULL);
            }

            for(i=0; i<subCount; i++){
                struct Subscriber *sub = sub_arr[i];
                pthread_join(*sub->TID, NULL);
            }

            Exit = 1;
            pthread_join(cleanup, NULL);
            
        }
    }

}


int main(int argc, char *argv[]){

    int i;
    for(i=0; i<MAX_TOPICS; i++){

        topics[i] = NULL;
    }

    for(i=0; i<MAX_THREADS; i++){
        
        pub_arr[i] = NULL;
        sub_arr[i] = NULL;
        
    }

    CommandArguments(argv[1]);

    return 0;
}