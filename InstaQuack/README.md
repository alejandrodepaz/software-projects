Quacker is a server technology developed for a theoretical social media service InstaQuack -- a cross 
between Instagram and Twitter which allows users to post photos with a brief caption. The Quacker server 
employs a publisher/subscriber model, where publishers send information to the Quacker system and subscribers
receive information from the system based on the topics they've subscribed to. The Quacker system's core 
responsibility is to manage multiple publishers and subscribers in a responsive and scalable way; this acheived 
through an elegant combination of multithreading, synchronization, and file I/O. 

![Quacker Server Architecture](images/Quacker.png)
