import tkinter as tk
from tkinter import *
from tkinter.filedialog import askopenfilename
from PIL import Image, ImageTk, ImageDraw
from tkinter import messagebox
import math
import json


# initialize app
parking_app = tk.Tk()
parking_app.eval('tk::PlaceWindow %s center' % parking_app.winfo_toplevel())

# initialize global variables
spot_array = []
name_array = []
spot = None
spot_counter = 1

bluespot_array = []
bluespot_names = []
bluespot = None
bluespot_counter = 1
ratio = None
distance = None
scale_array = []

canvas = None
blue_canvas= None
processed = True
blue_processed = True
master_frame = None

file_name = None
destroy = True


class Spot: # 'Spot' class; holds data for each processed spot by maintaining pointers to respective object attributes.

    def __init__(self):

        self.id = None #this is the camera ID
        self.association = None
        self.verify = None
        self.name_reference = None

        #this array holds the actual (x,y) coordinate pairs
        self.array = []

        #these arrays hold reference pointers to the actual canvas objects of points and lines
        self.points_array = []
        self.line_array = []

    def id_get(self):
        return self.id

    def association_get(self):
        return self.association

    def verify_get(self):
        return self.verify


def file_popup():

    global file_name

    popup = tk.Tk()
    popup.eval('tk::PlaceWindow %s center' % parking_app.winfo_toplevel())

    def end_popup():
        global file_name

        file_name = str(file_entry.get())
        finish_cb()
        popup.destroy()

    popup.wm_title("File Name")
    label = tk.Label(popup, text = "Please Specify File Name")
    label.grid(row = 0, column = 0, padx = 2)
    file_entry = tk.Entry(popup)
    file_entry.grid(row = 0, column = 1)
    button = tk.Button(popup, text = "Finish", command = end_popup)
    button.grid(row = 2, columnspan = 2 )

    popup.mainloop()


def finish_cb(): # 'finish' button callback function

    global file_name, spot_array, bluespot_array

    counter = 1
    data_dict = {}

    # write the data of each spot object to an existing json file
    if len(bluespot_array) == 0:
        for spot in spot_array:

            data_dict['a' + str(counter)] = []
            data_dict['a' + str(counter)].append({"associationID": spot.association,
                                                "cameraID": spot.id,
                                                "cameraLocation": str(spot.array),
                                                "blueprintLocation": str(bluespot.array)})

            counter += 1

        with open(file_name + ".json", 'w+') as outfile:
            json.dump(data_dict, outfile, indent = 4)


    else:

        for spot in spot_array:
            for bluespot in bluespot_array:
                if spot.association == bluespot.association:
                    data_dict['a' + str(counter)] = []
                    data_dict['a' + str(counter)].append({"associationID": spot.association,
                                                      "cameraID": spot.id,
                                                      "cameraLocation": spot.array,
                                                      "blueprintLocation": bluespot.array})

            counter += 1


        with open(file_name + ".json", 'w+') as outfile:
            json.dump(data_dict, outfile, indent = 4)



def process_cb(): # 'process spot' button callback function

    global canvas, blue_canvas, id_entry, association_entry, verify_entry, spot, spot_array, processed, bluespot, bluespot_array, blue_processed

    if spot != None:

        # assign respective Spot object attributes to the current entry values
        spot.id = str(id_entry.get())
        spot.association = str(association_entry.get())
        spot.verify = str(verify_entry.get())

        min_x = 1000000000
        min_y = 1000000000

        for i in range(len(spot.array)):

            if spot.array[i][0] < min_x:
                min_x = spot.array[i][0]

            if spot.array[i][1] < min_y:
                min_y = spot.array[i][1]


        # place association ID in spot box for referencing
        spot_name = canvas.create_text(min_x + 20, min_y + 20, font = ('Helvetica', 12), text = str(spot.association_get()))
        spot.name_reference = spot_name


        spot_array.append(spot)

        for i in spot.points_array:
            canvas.itemconfig(i, tags = str(spot.association))

        for j in spot.line_array:
            canvas.itemconfig(j, tags = str(spot.association))

        #clear entry fields
        id_entry.delete(0, END)
        association_entry.delete(0, END)
        verify_entry.delete(0, END)

        spot = None
        processed = True

    elif bluespot != None:

        # assign respective Spot object attributes to the current entry values
        bluespot.id = str(id_entry.get())
        bluespot.association = str(association_entry.get())
        bluespot.verify = str(verify_entry.get())

        min_x = 1000000000
        min_y = 1000000000

        for i in range(len(bluespot.array)):

            if bluespot.array[i][0] < min_x:
                min_x = bluespot.array[i][0]

            if bluespot.array[i][1] < min_y:
                min_y = bluespot.array[i][1]


        # label association ID in current spot box for referencing
        spot_name = blue_canvas.create_text(min_x + 20, min_y + 20, font = ('Helvetica', 12), text = str(bluespot.association_get()))
        bluespot.name_reference = spot_name


        bluespot_array.append(bluespot)

        #clear entry fields
        id_entry.delete(0, END)
        association_entry.delete(0, END)
        verify_entry.delete(0, END)

        bluespot = None
        blue_processed = True



def blueprint_tool(event):

    global bluespot, bluespot_counter, blue_canvas, processed, blue_processed

    if processed == False:

        tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")
    else:

    # initialize a new Spot object, assuming there isn't an unprocessed spot currently being processed
        if bluespot == None:
            bluespot = Spot()
            bluespot_counter += 1
            blue_processed = False

        if len(bluespot.array) < 4:
            x, y = blue_canvas.canvasx(event.x), blue_canvas.canvasy(event.y)
            point = blue_canvas.create_rectangle(x, y, x, y)
            bluespot.points_array.append(point)
            bluespot.array.append((x,y))

        # if an attempt to add a fifth point is made before the previous spot has been processed, display an error message
        else:
            tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")

        # system for creating lines that connect the four corners of the points to create a box. Kinda sketch but works.
        if len(bluespot.array) >= 2:

            line = blue_canvas.create_line(bluespot.array[len(bluespot.array) - 2][0], bluespot.array[len(bluespot.array) - 2][1], bluespot.array[-1][0], bluespot.array[-1][1])
            bluespot.line_array.append(line)

            if len(bluespot.array) == 4:
                line = blue_canvas.create_line(bluespot.array[0][0], bluespot.array[0][1], bluespot.array[-1][0], bluespot.array[-1][1])
                bluespot.line_array.append(line)



def blueprint_scale_tool(event): # event function for labeling reference line for scaling on blueprint

    global processed, blue_processed, blue_canvas, scale_array

    if processed == False or blue_processed == False:

        tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")

    else:

        if len(scale_array) < 2:
            x, y = blue_canvas.canvasx(event.x), blue_canvas.canvasy(event.y)
            point = blue_canvas.create_rectangle(x, y, x, y)
            blue_canvas.itemconfig(point, fill = 'red')
            scale_array.append([point, x, y])
        else:
            tk.messagebox.showerror("Placement Error","Point Placement Limit Reached, Please Submit Scale Value")


        if len(scale_array) == 2:
            line = blue_canvas.create_line(scale_array[0][1], scale_array[0][2], scale_array[1][1], scale_array[1][2])
            blue_canvas.itemconfig(line, fill = 'red')
            scale_array.append(line)
            distance_calc(scale_array[0][1], scale_array[1][1], scale_array[0][2], scale_array[1][2])

def scale_undo(): # callback function for undo button used in scaling toolbox

    global scale_array

    if len(scale_array) >= 1:

        if len(scale_array) == 1:
            blue_canvas.delete(scale_array.pop()[0])

        else:
            blue_canvas.delete(scale_array.pop())
            blue_canvas.delete(scale_array.pop()[0])


def distance_calc(x1, x2, y1, y2): # simple distance calculator function

    global distance

    distance = math.sqrt(((x2 - x1)**2) + (y2 - y1)**2)


def selection_tool(event): # selection tool function (how the points and lines are plotted).

    global spot, spot_counter, canvas, blue_processed, processed

    if blue_processed == False:
        tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")

    else:

        # initialize a new Spot object, assuming there isn't an unprocessed spot currently being processed
        if spot == None:
            spot = Spot()
            spot_counter += 1
            processed = False

        if len(spot.array) < 4:
            x, y = canvas.canvasx(event.x), canvas.canvasy(event.y)
            point = canvas.create_rectangle(x, y, x, y)
            spot.points_array.append(point)
            spot.array.append((x,y))

        # if an attempt to add a fifth point is made before the previous spot has been processed, display an error message
        else:
            tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")


        # system for creating lines that connect the four corners of the points to create a box. Kinda sketch but works.
        if len(spot.array) >= 2:

            line = canvas.create_line(spot.array[len(spot.array) - 2][0], spot.array[len(spot.array) - 2][1], spot.array[-1][0], spot.array[-1][1])
            spot.line_array.append(line)

            if len(spot.array) == 4:

                line = canvas.create_line(spot.array[0][0], spot.array[0][1], spot.array[-1][0], spot.array[-1][1])
                spot.line_array.append(line)



def undo_cb(): # 'undo' button callback function

    global spot, bluespot, processed, blue_processed, canvas, blue_canvas

    if spot != None and processed == False:

        # delete recently placed point and edge incident with point that was just deleted, assuming there there is more than one point
        if len(spot.array) >= 1:
            if len(spot.array) == 1:
                canvas.delete(spot.points_array.pop())
                spot.array.pop()

            elif len(spot.array) == 4:
                canvas.delete(spot.line_array.pop())
                canvas.delete(spot.line_array.pop())
                canvas.delete(spot.points_array.pop())
                spot.array.pop()

            else:
                canvas.delete(spot.points_array.pop())
                spot.array.pop()
                canvas.delete(spot.line_array.pop())


    elif bluespot != None and blue_processed == False:

        # delete recently placed point and edge incident with point that was just deleted, assuming there there is more than one point
        if len(bluespot.array) >= 1:
            if len(bluespot.array) == 1:
                blue_canvas.delete(bluespot.points_array.pop())
                bluespot.array.pop()

            elif len(bluespot.array) == 4:
                blue_canvas.delete(bluespot.line_array.pop())
                blue_canvas.delete(bluespot.line_array.pop())
                blue_canvas.delete(bluespot.points_array.pop())
                bluespot.array.pop()

            else:
                blue_canvas.delete(bluespot.points_array.pop())
                bluespot.array.pop()
                blue_canvas.delete(bluespot.line_array.pop())

def delete_cb():

    global spot_array, delete_entry, processed, bluespot_array, blue_processed, canvas, blue_canvas

    '''
    assuming there has been at least one spot processed, delete all instances of the specified object from the canvas,
    and remove the respective Spot object from the list of spots that will ultimately be written to the file
    '''

    if (len(spot_array) != 0 and processed == True) or (len(bluespot_array) != 0 and blue_processed == True):

        for spot in spot_array:
            if spot.association_get() == delete_entry.get():
                for i in range(len(spot.array)):
                    canvas.delete(spot.line_array.pop())
                    canvas.delete(spot.points_array.pop())

                canvas.delete(spot.name_reference)
                spot_array.remove(spot)

        for bluespot in bluespot_array:
            if bluespot.association_get() == delete_entry.get():
                for i in range(len(bluespot.array)):
                    blue_canvas.delete(bluespot.line_array.pop())
                    blue_canvas.delete(bluespot.points_array.pop())

                blue_canvas.delete(bluespot.name_reference)
                bluespot_array.remove(bluespot)


    else:
        tk.messagebox.showerror("Process Error","Please Process Previous Spot Before Attempting to Continue")




def blueprint_select_cb():

    global master_frame, blue_canvas, distance, ratio, scale_array, pictures

    def calculate_ratio():

        global distance, ratio

        if distance != None:

            conversion_value = float(scale_entry.get())
            ratio = conversion_value/distance
            scale_entry.delete(0, END)

            for i in range(3):
                if i == 2:
                    blue_canvas.delete(scale_array[i])
                else:
                    blue_canvas.delete(scale_array[i][0])

        else:
            tk.messagebox.showerror("Calculation Error","Please Specify a Relative Distance on Blueprint")




    blueprint_frame = tk.Frame(master_frame, width = 500, height = 500, padx = 5)

    blue_canvas = tk.Canvas(blueprint_frame, width = 500, height = 500)

    scale_frame = tk.Frame(blueprint_frame, width = 500)

    path = tk.filedialog.askopenfilename()
    image = Image.open(path)
    photo = ImageTk.PhotoImage(image)
    photo_width, photo_height = image.size
    photo.image = photo

    sbarV = Scrollbar(blueprint_frame, orient = VERTICAL)
    sbarH = Scrollbar(blueprint_frame, orient = HORIZONTAL)

    sbarV.config(command = blue_canvas.yview)
    sbarH.config(command = blue_canvas.xview)

    blue_canvas.config(yscrollcommand = sbarV.set)
    blue_canvas.config(xscrollcommand = sbarH.set)

    sbarV.pack(side=RIGHT, fill = Y)
    sbarH.pack(side=BOTTOM, fill = X)

    blueprint_label = tk.Label(blueprint_frame, text = "Blueprint Diagram")
    blueprint_label.pack(side = TOP)

    blue_canvas.pack(expand = YES, fill = BOTH)
    blue_canvas.config(scrollregion=(0,0,photo_width, photo_height))
    blue_canvas.create_image(0, 0, anchor = NW, image = photo)
    blue_canvas.bind('<Button-1>', blueprint_tool)
    blue_canvas.bind('<Shift-Button-1>', blueprint_scale_tool)

    blueprint_frame.pack(side = RIGHT, expand = YES)

    scale_label = tk.Label(scale_frame, text = "Scale Value in Feet")
    scale_label.grid(row = 0, column = 0, pady = 2, padx = 2)
    scale_entry = tk.Entry(scale_frame)
    scale_entry.grid(row = 0, column = 1, pady = 2)

    scale_button = tk.Button(scale_frame, text = "Submit", command = calculate_ratio)
    scale_button.grid(row = 0, column = 2)

    scale_undo_button = tk.Button(scale_frame, text = "Scale Undo", command = scale_undo)
    scale_undo_button.grid(row = 1, columnspan = 3)
    scale_frame.pack(side = BOTTOM)



    parking_app.eval('tk::PlaceWindow %s center' % parking_app.winfo_toplevel())



def image_select_cb(): # 'image selection' button callback function
    global canvas, master_frame, logo_label, destroy
    # prompt the user for the image file they wish to use
    path = tk.filedialog.askopenfilename()
    image = Image.open(path)
    photo2 = ImageTk.PhotoImage(image)
    photo_width, photo_height = image.size
    photo2.image = photo2

    '''
    If the 'Upload' button has been invoked, create a new blank file which will be written to. From here, create the
    entries where the Spot attributes are listed, as well as the "Process Spot" button, "Delete" button, and "Undo"
    button.
    '''
    if destroy == True:
        logo_canvas.destroy()
        destroy = False

    master_frame = tk.Frame(parking_app, relief = SUNKEN, width = 1000, height = 1200)
    master_frame.pack(side = LEFT, expand = YES)

    image_frame = tk.Frame(master_frame, width = 500, height = 500, padx = 5)

    canvas = tk.Canvas(image_frame, width = 500, height = 500)

    sbarV = Scrollbar(image_frame, orient = VERTICAL)
    sbarH = Scrollbar(image_frame, orient = HORIZONTAL)

    sbarV.config(command = canvas.yview)
    sbarH.config(command = canvas.xview)

    canvas.config(yscrollcommand = sbarV.set)
    canvas.config(xscrollcommand = sbarH.set)

    sbarV.pack(side=RIGHT, fill = Y)
    sbarH.pack(side=BOTTOM, fill = X)

    image_label = tk.Label(image_frame, text = "Camera Image")
    image_label.pack(side = TOP)

    blank_label = tk.Label(image_frame, text = '')
    blank_label.pack(side = BOTTOM, fill = Y)

    canvas.pack(expand = YES, fill = BOTH)
    canvas.config(scrollregion=(0,0,photo_width, photo_height))
    canvas.create_image(0, 0, anchor = NW, image = photo2)
    canvas.bind('<Button-1>', selection_tool)

    filler_label = tk.Label(image_frame, text = "", pady = 2)
    filler_label.pack(side = BOTTOM)


    image_frame.pack(side = LEFT, expand=YES, padx = 2)


    blueprint_label = tk.Label(frame, text = "Select Corresponding Blueprint Image: ").grid(row = 1, column = 2,sticky = W,  pady = 20, padx = 10)
    blueprint_button = tk.Button(frame, text = "Blueprint", command = blueprint_select_cb).grid(row = 1, column = 3,sticky = W,  pady = 20, padx = 5)

    send_label = tk.Label(frame, text = "Send Data: ")
    send_label.grid(row = 1, column = 4,sticky = W,  pady = 20, padx = 10)
    send_button = tk.Button(frame, text='File', command=file_popup)
    send_button.grid(row=1, column=5, sticky = W, pady = 20, padx = 5)



    #########################################################################



    global id_entry, verify_entry, association_entry, delete_entry

    # Initializing all the labels/buttons. Nothing special.
    tools_frame = tk.Frame(parking_app, width = 40, highlightbackground= 'purple', highlightcolor = 'purple', highlightthickness = 5)

    process_button = tk.Button(tools_frame, text = "Process Spot", command = process_cb)
    process_button.grid(row = 0, column = 0, pady = 5)

    association_id = tk.Label(tools_frame, text = "Association ID:")
    association_id.grid(row = 1, column = 0)
    association_entry = tk.Entry(tools_frame)
    association_entry.grid(row = 2, column = 0)

    verify_label = tk.Label(tools_frame, text = "Verify Filled (Boolean):")
    verify_label.grid(row = 3, column = 0)
    verify_entry = tk.Entry(tools_frame)
    verify_entry.grid(row = 4, column = 0)

    id_label = tk.Label(tools_frame, text = "Specify Camera ID:")
    id_label.grid(row = 5, column = 0)
    id_entry = tk.Entry(tools_frame)
    id_entry.grid(row = 6, column = 0)

    delete_button = tk.Button(tools_frame, text = "Delete Spot", command = delete_cb)
    delete_button.grid(row = 8, column = 0)
    delete_entry = tk.Entry(tools_frame)
    delete_entry.grid(row = 9, column = 0)

    undo_button = tk.Button(tools_frame, text = "Undo", command = undo_cb)
    undo_button.grid(row = 10, column = 0, pady = 20)

    tools_frame.pack(side = RIGHT, padx=2, expand = YES)

    parking_app.eval('tk::PlaceWindow %s center' % parking_app.winfo_toplevel())

# set up initial GUI labels and buttons. This includes 'image select' label/button and company logo
parking_app.title("SpotCheck Initializer")
frame = tk.Frame(parking_app, width = 200)

logo = ImageTk.PhotoImage(Image.open("SpotCheckLogo.png"))
logo_canvas = tk.Canvas(frame, width = 250, height = 250)
logo_canvas.grid(row = 0, column = 0, pady = 5, columnspan = 2)
logo_canvas.create_image(0,0, image = logo, anchor = NW)

tk.Label(frame, text = "Select an Image for Processing:").grid(row = 1, column = 0,sticky = W,  pady = 20, padx = 10)
tk.Button(frame, text='Image Upload', command=image_select_cb).grid(row = 1, column=1, sticky = W, pady = 20, padx = 5)

frame.pack(side = TOP, pady = 2)


if __name__ == "__main__":
    parking_app.mainloop()
