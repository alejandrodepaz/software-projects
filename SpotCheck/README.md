#SpotCheck Initializing-GUI

This is the preliminary code for the GUI which will be used to initialize camera data. Usage information is provided below.

Application Description: This GUI utilizes the python Tkinter and PIL libraries to construct a simple application that can be used to 
label spaces that qualify as 'parking spots' in a provided image. Once all appropriate spots have been labeled and processed, the data 
values, in terms of (x,y) coordinate pairs, are stored in a dictionary which will ultimately be written to a json file for transfer to 
our main server. Accompanying the digital image of the parking lot/zone of interest, a blueprint image may also be uploaded for further 
labeling; the labeling of this blueprint will be done in such a manner that each respective spot in the diagram correlates directly with 
a spot in the digital image. Correlating spots will be paired under a specific association ID, and written to the JSON file specified as 
such.

*Note: Some of the Tkinter Methods used in this program are now deprecated, and only run apporpriately when executed using the Python IDLE
Environment.  

User Instructions:

i) To begin, the user may run the GUI via command line as follows: python3 ImageInterface.py - Note: you must be in the appropriate file directory before making this call.

ii) Once the application has been opened, you will be prompted to select an image. This image should be the digital image of the parking lot/zone of interest.

iii) With the appropriate image selected, the user may now choose to begin labeling and processing parking spots. This is done by left-clicking on the image itself, which has been layered on top of a canvas object which will store the location of the click as well as label the location of the click with a pixel-sized rectangle object. Once two or more points have been labeled, a series of lines will connect the points in such a way that you can gauge the accuracy of the parallelogram you are constructing. These lines are not part of the data which will be stored for future use.

*Note: it is important that you do not label two consecutive points such that the two points are in opposite corners. This will create a diagonal line, as the lines that outline the parallelogram are drawn in an order such that they connect consecutively added points. Any other order of labeling will suffice, though.

iv) At any point in time, the user may select a blueprint image using the button labeled "Select Blueprint Image" at the top of the mainframe. This button will open another seperate canvas instance which can also be labeled in the same manner as the first image.

v) Additionally, a toolbox on the righthand side of the GUI will feature 3 entry boxes that should be self explanatory, as well as a delete and undo button. The "undo" button allows the user to remove previously added points (as well as any edges incident with them) one at a time. The "delete" button allows the user to specify a spot by its association ID (which will be labeled inside the box once processed), and delete it from the canvas and data set.

vi) Finally, writing to the json file is done via the button labeled "Send Data" at the top of the mainframe. When evoked, a popup window will prompt the user to enter a file name. Do not specify the file type, as the program does this automatically. Simply specify a string without spaces, and this will be the name of the json file which the data is written to.
