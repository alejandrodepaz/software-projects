# Currency Converter App

Simple android application that allows users to convert currencies using current exchange rates supplied by the [European Central Bank's api](https://exchangeratesapi.io/). Included is the ability to quickly
compute standard tip percentages for the converted value.

![Alt Text](./Currency_Converter.gif)
