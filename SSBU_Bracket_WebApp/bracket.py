from flask import Flask, render_template, flash, redirect, url_for, session, request, logging, g,jsonify
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
import json
import sqlite3
import copy


app = Flask(__name__)

@app.route('/')
@app.route('/index', methods = ["GET", "POST"])
def index():
    return render_template('smashBracket.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/nav')
def nav():
    return render_template('main_nav.html')

@app.route('/bracket')
def bracket():
    return render_template('bracket.html')

@app.route('/leaderboard')
def leaderboard():
    return render_template('leaderboard.html')

@app.route('/fromDB', methods = ['GET'])
def _fromDB():
    conn = sqlite3.connect('ssbu.db')
    c = conn.cursor()

    c.execute("SELECT * FROM players ORDER BY wins")
    players = c.fetchall()

    plyrs = []
    for row in players:
        plyrs.append({'id':row[0], 'name':row[1], 'wins':row[2], 'loses':row[3], 'tourney_wins':row[4]})

    c.execute("SELECT * FROM tourneys")
    tourneys = c.fetchall()

    trnys = []
    for row in tourneys:
        trnys.append({'id':row[0], 'name':row[1], 'first':row[2], 'second':row[3], 'third':row[4]})

    print(plyrs)
    print(trnys)

    conn.commit()
    conn.close()

    return jsonify(players=plyrs,tourneys=trnys)

@app.route("/toDB", methods = ['POST'])
def _toDB():
    players = json.loads(request.form['players'])
    #Scrub player names of spaces and return characters
    for player in players:
        if player[0] != None:
            player[0] = player[0].replace("\n", "")
            player[0] = player[0].strip()
        if player[1] != None:
            player[1] = player[1].replace("\n", "")
            player[1] = player[1].strip()
    scores = json.loads(request.form['scores'])
    bracket_title = request.form['bracket_title']

    conn = sqlite3.connect('ssbu.db')
    c = conn.cursor()

    c.execute('''CREATE TABLE IF NOT EXISTS players (
    id integer PRIMARY KEY,
    name text NOT NULL UNIQUE,
    wins integer DEFAULT 0,
    loses integer DEFAULT 0,
    tourney_wins DEFAULT 0,
    UNIQUE (name)
    )''')

    c.execute('''CREATE TABLE IF NOT EXISTS tourneys (
    id integer PRIMARY KEY,
    name text NOT NULL UNIQUE,
    first text,
    second text,
    third text
    )''')
    conn.commit()

    first, second, third, wins, loses = scrub(players,scores)

    wins.pop('None', None)
    wins.pop(None, None)
    loses.pop('None', None)
    loses.pop(None, None)

    for key,value in wins.items():
        c.execute("SELECT * FROM players WHERE name = ?", (key,))
        if c.fetchone() == None:
            c.execute("INSERT INTO players (name, wins) VALUES (?,?)", (key, wins[key],))
        else:
            c.execute("SELECT wins FROM players WHERE name = ?", (key,))
            num_wins = c.fetchone()
            num_wins = num_wins[0]
            num_wins += wins[key]
            c.execute("UPDATE players SET wins = ? WHERE name = ?", (num_wins, key,))

    for key,value in loses.items():
        c.execute("SELECT * FROM players WHERE name = ?", (key,))
        if c.fetchone() == None:
            c.execute("INSERT INTO players (name, loses) VALUES (?,?)", (key, loses[key],))
        else:
            c.execute("SELECT loses FROM players WHERE name = ?", (key,))
            num_loses = c.fetchone()
            num_loses = num_loses[0]
            num_loses += loses[key]
            c.execute("UPDATE players SET loses = ? WHERE name = ?", (num_loses, key,))

    c.execute("SELECT tourney_wins FROM players WHERE name = ?", (first,))
    t_wins = c.fetchone()
    t_wins = t_wins[0]
    t_wins += 1
    c.execute("UPDATE players SET tourney_wins = ? WHERE name = ?", (t_wins, first,))

    c.execute("SELECT * FROM tourneys WHERE name=?", (bracket_title,))
    if c.fetchone() == None:
        c.execute("INSERT INTO tourneys (name, first, second, third) VALUES (?,?,?,?)", (bracket_title,first,second,third,))

    bs = c.fetchall()
    c.execute("SELECT * FROM players ORDER BY wins")
    players = c.fetchall()

    c.execute("SELECT * FROM tourneys")
    tourneys = c.fetchall()

    #c.execute("DROP TABLE players")

    conn.commit()
    conn.close()
    return 'ok'


def scrub(players, scores):
    '''This function takes players and results array from the jquery bracket template
        and returns the the winner, 2nd place, 3rd place and wins and loses Dictionaries
         for each player

        Return: (winners array, losers array, finals array, the winner, wins dict, loses dict)

        Note: The finals array is peculiar, only the first tuple really matters

        Note: Dictionaries contain entry: value='None', ingore This

        Note: Should probably scrub values of dictionaries'''

    length = len(players)
    tourney = copy.deepcopy(scores)
    winners = tourney[0]
    the_winner = 0
    losers = tourney[1]
    finals = tourney[2]
    winners[0] = players;
    wins = {}
    loses = {}

    for i in range(length):
        wins[players[i][0]] = 0
        loses[players[i][0]] = 0
        wins[players[i][1]] = 0
        loses[players[i][1]] = 0
    wins['None'] = 0
    loses['None'] = 0

    #The following for loop iterates the winners side
    for i in range(len(winners)-1):
        if i==0:
            lround = 0
        elif i==1:
            lround = 1
            swap = True
        elif i==2:
            lround = 3
            swap = False
        elif i==3:
            lround = 5
            swap = True
        elif i==4:
            lround = 7
            swap = False
        elif i==5:
            lround = 9
            swap = True
        elif i==6:
            lround = 11
            swap = False
        #print("starting {}  i loop\n".format(i))
        cur_round = winners[i]
        cur_length = len(cur_round)
        for j in range(cur_length):
            #print("starting {} j loop\n".format(j))
            #print(scores[0][i][j][0],scores[0][i][j][1])
            if scores[0][i][j][1] == None:
                if (j%2) == 0:
                    winners[i+1][j//2][0] = winners[i][j][0]
                else:
                    winners[i+1][j//2][1] = winners[i][j][0]
            elif scores[0][i][j][0]== None:
                if (j%2) == 0:
                    winners[i+1][j//2][0] = winners[i][j][1]
                else:
                    winners[i+1][j//2][1] = winners[i][j][1]

            elif scores[0][i][j][0] > scores[0][i][j][1]:
                if (j%2) == 0:
                    winners[i+1][j//2][0] = winners[i][j][0]
                    if lround == 0:
                        losers[lround][j//2][0] = winners[i][j][1]
                    else:
                        swap_help = cur_length - 1
                        swap_place = swap_help - j
                        if swap==True:
                            losers[lround][swap_place][1] = winners[i][j][1]
                        else:
                            losers[lround][j][1] = winners[i][j][1]


                    wins['{}'.format(winners[i][j][0])] += 1
                    loses['{}'.format(winners[i][j][1])] += 1
                else:
                    winners[i+1][j//2][1] = winners[i][j][0]
                    if lround == 0:
                        losers[lround][j//2][1] = winners[i][j][1]
                    else:
                        swap_help = cur_length - 1
                        swap_place = swap_help - j
                        if swap==True:
                            losers[lround][swap_place][1] = winners[i][j][1]
                        else:
                            losers[lround][j][1] = winners[i][j][1]

                    wins['{}'.format(winners[i][j][0])] += 1
                    loses['{}'.format(winners[i][j][1])] += 1

            else:
                if (j%2) == 0:
                    winners[i+1][j//2][0] = winners[i][j][1]
                    if lround == 0:
                        losers[lround][j//2][0] = winners[i][j][0]
                    else:
                        swap_help = cur_length - 1
                        swap_place = swap_help - j
                        if swap==True:
                            losers[lround][swap_place][1] = winners[i][j][1]
                        else:
                            losers[lround][j][1] = winners[i][j][1]

                    loses['{}'.format(winners[i][j][0])] += 1
                    wins['{}'.format(winners[i][j][1])] += 1

                else:
                    winners[i+1][j//2][1] = winners[i][j][1]
                    if lround == 0:
                        losers[lround][j//2][1] = winners[i][j][0]
                    else:
                        swap_help = cur_length - 1
                        swap_place = swap_help - j
                        if swap==True:
                            losers[lround][swap_place][1] = winners[i][j][1]
                        else:
                            losers[lround][j][1] = winners[i][j][1]
                    loses['{}'.format(winners[i][j][0])] += 1
                    wins['{}'.format(winners[i][j][1])] += 1

    #Conditional sets winners finals
    if scores[0][-1][0][0] > scores[0][-1][0][1]:
        finals[0][0][0] = winners[-1][0][0]
        losers[-1][0][1] = winners[-1][0][1]
        loses['{}'.format(winners[-1][0][1])] += 1
        wins['{}'.format(winners[-1][0][0])] += 1
    else:
        finals[0][0][0] = winners[-1][0][1]
        losers[-1][0][1] = winners[-1][0][0]
        loses['{}'.format(winners[-1][0][0])] += 1
        wins['{}'.format(winners[-1][0][1])] += 1

    #The following for loop iterates the losers side
    for i in range(len(losers)-1):

        cur_length = len(losers[i])

        for j in range(cur_length):
            if (i%2)==0:
                if scores[1][i][j][1] == None:
                    losers[i+1][j][0] = losers[i][j][0]
                elif scores[1][i][j][0] == None:
                    losers[i+1][j][0] = losers[i][j][1]
                elif scores[1][i][j][0] > scores[1][i][j][1]:
                    losers[i+1][j][0] = losers[i][j][0]
                    wins['{}'.format(losers[i][j][0])] += 1
                    loses['{}'.format(losers[i][j][1])] += 1
                else:
                    losers[i+1][j][0] = losers[i][j][1]
                    wins['{}'.format(losers[i][j][1])] += 1
                    loses['{}'.format(losers[i][j][0])] += 1
            else:
                if (j%2) == 0:
                    if scores[1][i][j][1] == None:
                        losers[i+1][j//2][0] = losers[i][j][0]
                    elif scores[1][i][j][0] == None:
                        losers[i+1][j//2][0] = losers[i][j][1]
                    elif scores[1][i][j][0] > scores[1][i][j][1]:
                        losers[i+1][j//2][0] = losers[i][j][0]
                        wins['{}'.format(losers[i][j][0])] += 1
                        loses['{}'.format(losers[i][j][1])] += 1
                    else:
                        losers[i+1][j//2][0] = losers[i][j][1]
                        wins['{}'.format(losers[i][j][1])] += 1
                        loses['{}'.format(losers[i][j][0])] += 1
                else:
                    if scores[1][i][j][1] == None:
                        losers[i+1][j//2][1] = losers[i][j][0]
                    elif scores[1][i][j][0] == None:
                        losers[i+1][j//2][1] = losers[i][j][1]
                    elif scores[1][i][j][0] > scores[1][i][j][1]:
                        losers[i+1][j//2][1] = losers[i][j][0]
                        wins['{}'.format(losers[i][j][0])] += 1
                        loses['{}'.format(losers[i][j][1])] += 1
                    else:
                        losers[i+1][j//2][1] = losers[i][j][1]
                        wins['{}'.format(losers[i][j][1])] += 1
                        loses['{}'.format(losers[i][j][0])] += 1

    #Conditional sets losers finals
    if scores[1][-1][0][0] > scores[1][-1][0][1]:
        finals[0][0][1] = losers[-1][0][0]
        wins['{}'.format(losers[-1][0][0])] += 1
        loses['{}'.format(losers[-1][0][1])] += 1
        the_third = losers[-1][0][1]
    else:
        finals[0][0][1] = losers[-1][0][1]
        wins['{}'.format(losers[-1][0][1])] += 1
        loses['{}'.format(losers[-1][0][0])] += 1
        the_third = losers[-1][0][0]

    #Conditional for grand finals 2
    if len(finals) == 2:
        if scores[2][1][0][0] > scores[2][1][0][1]:
            wins['{}'.format(finals[0][0][0])] += 1
            loses['{}'.format(finals[0][0][1])] += 1
            the_winner = finals[0][0][0]
            the_second = finals[0][0][1]
        else:
            wins['{}'.format(finals[0][0][1])] += 1
            loses['{}'.format(finals[0][0][0])] += 1
            the_winner = finals[0][0][1]
            the_second = finals[0][0][0]

    #Conditional for grand finals
    if scores[2][0][0][0] > scores[2][0][0][1]:
        wins['{}'.format(finals[0][0][0])] += 1
        loses['{}'.format(finals[0][0][1])] += 1
        if the_winner==0:
            the_winner = finals[0][0][0]
            the_second = finals[0][0][1]
    else:
        wins['{}'.format(finals[0][0][1])] += 1
        loses['{}'.format(finals[0][0][0])] += 1
        if the_winner==0:
            the_winner = finals[0][0][1]
            the_second = finals[0][0][0]

    return the_winner, the_second, the_third, wins, loses



if __name__ == '__main__':
    app.secret_key='secret123'
    app.run(debug=True)
