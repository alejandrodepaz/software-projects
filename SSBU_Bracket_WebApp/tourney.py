class Tournament:
    start_players = []
    w_bracket = [[], [], [], [ [],[] ], []]
    l_bracket = [[], [], [ [],[] ], [ [],[] ]]
    w_teirs = 0
    l_teirs = 0
    t_size = 0

    def __init__(self, players):
        self.start_players = players
        self.t_size = len(players)
        self.get_dim()


    def get_dim(self):
        size = self.t_size

        #self.bracket[1] = [] # [winner, [winner, 2nd place]]
        #self.bracket[2] = [] # [winner, [winner, 2nd aplce], [1st, 2nd, 3rd, 4th]]]
        if size == 8:
            self.w_teirs = 5
            self.l_teirs = 4

            for i in range(1, 5):
                self.w_bracket[4].append(self.start_players[2*i-2:2*i])

        elif size == 16:
            self.w_teirs = 6
            self.l_teirs = 5
            self.w_bracket.append([])
            self.l_bracket.extend(([], []))

            for i in range(1, 5):
                self.w_bracket[4].append([])


            for i in range(1, 9):
                self.w_bracket[5].append(self.start_players[2*i-2:2*i])

            for i in range(1, 5):
                self.l_bracket[4].append([])
                self.l_bracket[5].append([])


        elif size == 32:
            self.w_teirs = 7
            self.l_teirs = 6
            self.w_bracket.extend(([], []))
            self.l_bracket.extend(([], [], [], []))


            for i in range(1, 5):
                self.w_bracket[4].append([])

            for i in range(1, 9):
                self.w_bracket[5].append([])

            for i in range(1, 17):
                self.w_bracket[6].append(self.start_players[2*i-2:2*i])

            for i in range(1, 5):
                self.l_bracket[4].append([])
                self.l_bracket[5].append([])

            for i in range(1, 9):
                self.l_bracket[6].append([])
                self.l_bracket[7].append([])



        elif size == 64:
            self.w_teirs = 8
            self.l_teirs = 7
            self.w_bracket.extend(([], [], []))
            self.l_bracket.extend(([], [], [], [], [], []))


            for i in range(1, 5):
                self.w_bracket[4].append([])

            for i in range(1, 9):
                self.w_bracket[5].append([])

            for i in range(1, 17):
                self.w_bracket[6].append([])

            for i in range(1, 33):
                self.w_bracket[7]. append(self.start_players[2*i-2:2*i])

            for i in range(1, 5):
                self.l_bracket[4].append([])
                self.l_bracket[5].append([])


            for i in range(1, 9):
                self.l_bracket[6].append([])
                self.l_bracket[7].append([])

            for i in range(1, 17):
                self.l_bracket[8].append([])
                self.l_bracket[9].append([])


    def print_brack(self):
        for teir in self.w_bracket:
            print(teir)

        print("")

        for teir in self.l_bracket:
            print(teir)





list64 = ["a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h","a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h", "a", "b", "c", "d", "e", "f", "g", "h"]
list8 = ["a", "b", "c", "d", "e", "f", "g", "h"]
list16 = ["a", "b", "c", "d", "e", "f", "g", "h","a", "b", "c", "d", "e", "f", "g", "h"]
list32 = ["a", "b", "c", "d", "e", "f", "g", "h","a", "b", "c", "d", "e", "f", "g", "h","a", "b", "c", "d", "e", "f", "g", "h","a", "b", "c", "d", "e", "f", "g", "h"]
t1 = Tournament(list64)
t1.print_brack()
