#SSBU Bracket-Generator Web Application

This project features a web application which allows a user to generate a
customizable tournament-ready bracket for competitions being hosted for Super
Smash Bros. Ultimate.

Specifications:
- Server-side mechanics utilize the Python Flask web framework, while the
bracket itself is generated using the JQuery Bracket Library.

This project is still in the early stages of development, but it currently set
for completion by the end of March.

