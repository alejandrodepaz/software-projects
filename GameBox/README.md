# GameBox (Chess & Checkers)

How to run:

1) Enter the "GameFiles/" directory 

2) Enter command "make games"

3) Run executable using command "./games.exe"


#Menu:

The menu page will be the first display to pop up upon running GameBox. This menu simply acts
as a visual aid for displaying the available games in the GameBox. As you will notice, the available
games are "CHESS" and "CHECKERS". As a user, you may enter either CHESS or CHECKERS, exactly as is
spelled out in the menu, and press enter to run the respective game (or 'exit' in order to quit the
GameBox altogether). Upon completion of a game, you will be redirected to the Main Menu, where you 
can re-select a game of your choice -- or exit. The Main Menu will remain stagnant until a game is 
chosen or 'exit' is specified.  

#Checkers:
*Note: This Checkers is Player vs. Player -- there is no bot to play against. 
All the classic rules apply to this version of Checkers, though they are explained down below.
As usual, the white player always goes first. At start-up Player 1 will be prompted to choose a 
color; the user who is Player 1  can be arbitrarily decided between the current two users. The
board is then printed with the classic initial layout, 12 tiles of each color (24 tiles total). 
The player's respective colors are also listed as a friendly reminder, along with each player's 
score (which is initially zero). 

- Rules:
i) White player goes first.

ii) Turns alternate between white and red.
 
iii) Non-king pieces can only move forward along their diagonals, either right or left.

iv) If a tile is making a non-kill jump, it can only travel a maximum of one position on the 
diagonal. Moves are made as follows: the user is asked to choose an attack piece (meaning the
piece they would like to move) by specifying its position on the board (e.g. '23', meaning ROW 2
and COlUMN 3). The user is then asked to specify a destination position in the same format. Note
that if an invalid move is requested, you will be alerted and asked to try again until a valid
input is provided. If a valid move can be made, it must be made. The one twist is: if a kill move
is available, you do not have to take it. In orginal checkers, this is a forced rule which I think
limits a player's strategy; as such, it has been omitted.
  
v) Multiple kill-jumps can be made; the user can achieve this by entering multiple destination 
positions like follows: 12 23 34
 
vi) If a tile makes it to the opposite side of the board (ROW 7 for white or ROW 0 for red),
then this tile will be automatically 'kinged. A King can move both forwards and backwards along
its diagonals, as well as make multiple kill jumps that go forward and backwards.
 
vii) The game is over once a player takes all of the opponents tiles, or one of the players can
no long make a valid move (meaning they cannot move into an empty position or make a kill jump).

viii) Once the game is over, a small display will pop-up listing the winner. The user may press the
ENTER button to return to the Main Menu.    



#Chess:

White player always goes first! Players enter row,col pairs seperated with a press of the enter key.
Players will not be able to move pieces in incorrect ways or to invalid positions.
Chess is fully functional including ability to castle (enter '101') and pawn replacement on far side 
of board. Game will end when one of the kings has been captured or the player enters '-1' to cancel 
the game early.  Heads up, it's up to you to tell your opponent that they're in check... or not!
