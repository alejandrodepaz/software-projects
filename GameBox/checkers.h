#ifndef CHECKERS_H_
#define CHECKERS_H_
#include <iostream>
#include <string>
#include <vector>
#include <sstream>  
using namespace std;

//player struct with color (black or white) attribute
class Player{

    public:
    std::string color;
    std::string name;
    std::string king;
    std::string opponent;
    std::string opponent_king;
    int score;

};
class Checkers{
    public:

    std::vector<std::string> split(std::string str, char delimiter);

    void playerInit(Player &player1, Player &player2);

    void boardInit(std::string **game_board, int ROWS, int COLS);

    void printBoard(std::string **game_board, int ROWS, int COLS, Player first_player, Player second_player);

    int noValidMoves(std::string **matrix, int ROWS, int COLS, Player player);

    int checkGameOver(std::string **game_board,int ROWS, int COLS, Player player1, Player player2);

    int checkValidDirection(std::string **matrix, int ROWS, int COLS, int init_row, int init_col, std::vector< std::vector<int> > &moves, Player &player);

    int run(Player &player1, Player &player2);
};


#endif /* Checkers_H_ */