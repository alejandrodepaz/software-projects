
#include <iostream>
#include <string>
#include "reversi.h"

int main(){

    ReversiDimensions board;
    Player first_player;
    Player second_player;
    playerInit(&first_player, &second_player);
    std::cout << "Player 1 Color: " << first_player.color << "\n";
    std::cout << "Player 2 Color: " <<  second_player.color << "\n";
    boardInit(&board);
    std::cout << "Board Dimensions: " << board.ROWS << "x" << board.COLUMNS << "\n";

    //Declare player1 and player2 based on player using black tiles.
    if (first_player.color == 'b'){
        run(&board, &first_player, &second_player);
    } else{
        run(&board, &second_player, &first_player);
    }

}