#Othello (CIS 330: C/C++/Unix)

This is a simple console-based board game which models the physical board game
Othello. Listed below are the rules on how to play.

Rules:

1) To begin, the user may specify a board size using a single integer (e.g. 
entering '8' will generate an 8x8 board).

2) Next, because the white tile always goes first, the white player will 
be prompted to enter a 'ROW', again as a single integer, and a 'COLUMN' 
of the same type. These values must correspond to a position on the board
where you would like to place a tile of your respective color.

3) The position a player specifies must encompass an opponents pieces; in 
other words, Each piece played must be laid adjacent to an opponent's piece so
that the opponent's piece or a row of opponent's pieces is flanked by the new 
piece and another piece of the player's colour. 

4)All of the opponent's pieces between these two pieces are 'captured' and 
turned over to match the player's colour.

5) The game is over when a player has captured all of their opponents tiles
(flipped all of the opponents pieces so that only their pieces remain on the 
board), or once a player has no more valid moves (meaning they cannot legally
place a piece in a position that flips their opponents pieces). 
