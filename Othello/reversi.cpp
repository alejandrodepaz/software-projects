#include <iostream>
#include <string>
#include "reversi.h"

//direction checking function. 
int checkDirection(char **matrix, int COLS, int ROWS, int col_num, int row_num, char color, Direction direct, int *possible_rows, int *possible_cols, int *counter){

    int count = 0;

    int *potn_cols = nullptr;
    potn_cols = new int[COLS];

    int *potn_rows = nullptr;
    potn_rows = new int[ROWS];

    if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
        return 0;
    }
    if (matrix[row_num][col_num] != '.'){
        return 0;
    }

    //move to a position on the board given the direction requested in the parameter "direct".
    if (direct == Direction::up){
        row_num -= 1;
    } else if(direct == Direction::down){
        row_num += 1;
    } else if (direct == Direction::left){
        col_num -= 1;
    } else if(direct == Direction::right){
        col_num += 1;
    } else if (direct == Direction::upRight){
        row_num -= 1;
        col_num += 1;
    } else if(direct == Direction::upLeft){
        row_num -= 1;
        col_num -=1;
    } else if (direct == Direction::downRight){
        row_num += 1;
        col_num += 1;
    } else{
        if(direct == Direction::downLeft){
            row_num += 1;
            col_num -= 1;
        }
    }
    //if move is out of range of board, return 0 for a 'false move'
    if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
        return 0;
    }

    if ((matrix[row_num][col_num] == '.') || (matrix[row_num][col_num] == color)){
        return 0;
    } else{
        potn_cols[count] = col_num;
        potn_rows[count] = row_num;
        count += 1;
    }

    while(1){
        if (direct == Direction::up){
            row_num -= 1;
        } else if(direct == Direction::down){
            row_num += 1;
        } else if (direct == Direction::left){
            col_num -= 1;
        } else if(direct == Direction::right){
            col_num += 1;
        } else if (direct == Direction::upRight){
            row_num -= 1;
            col_num += 1;
        } else if(direct == Direction::upLeft){
            row_num -= 1;
            col_num -=1;
        } else if (direct == Direction::downRight){
            row_num += 1;
            col_num += 1;
        } else{
             if(direct == Direction::downLeft){
                row_num += 1;
                col_num -= 1;
             }
        }

        if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
            return 0;
        }
        
        if (matrix[row_num][col_num] == '.'){
            return 0;
        }
        if (matrix[row_num][col_num] == color){
            break;
        } else{
            potn_cols[count] = col_num;
            potn_rows[count] = row_num;
            count +=1;
        }
    }

    int k;
    for (k=0; k<count; k++){
        possible_cols[*counter] = potn_cols[k];
        possible_rows[*counter] = potn_rows[k];
        *counter += 1;
    }
    return 1;
}

int checkValidDirection(char **matrix, int COLS, int ROWS, int col_num, int row_num, char color, Direction direct){

    if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
        return 0;
    }
    if (matrix[row_num][col_num] != '.'){
        return 0;
    }

    //move to a position on the board given the direction requested in the parameter "direct".
    if (direct == Direction::up){
        row_num -= 1;
    } else if(direct == Direction::down){
        row_num += 1;
    } else if (direct == Direction::left){
        col_num -= 1;
    } else if(direct == Direction::right){
        col_num += 1;
    } else if (direct == Direction::upRight){
        row_num -= 1;
        col_num += 1;
    } else if(direct == Direction::upLeft){
        row_num -= 1;
        col_num -=1;
    } else if (direct == Direction::downRight){
        row_num += 1;
        col_num += 1;
    } else{
        if(direct == Direction::downLeft){
            row_num += 1;
            col_num -= 1;
        }
    }
    //if move is out of range of board, return 0 for a 'false move'
    if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
        return 0;
    }

    if ((matrix[row_num][col_num] == '.') || (matrix[row_num][col_num] == color)){
        return 0;
    }

    while(1){
        if (direct == Direction::up){
            row_num -= 1;
        } else if(direct == Direction::down){
            row_num += 1;
        } else if (direct == Direction::left){
            col_num -= 1;
        } else if(direct == Direction::right){
            col_num += 1;
        } else if (direct == Direction::upRight){
            row_num -= 1;
            col_num += 1;
        } else if(direct == Direction::upLeft){
            row_num -= 1;
            col_num -=1;
        } else if (direct == Direction::downRight){
            row_num += 1;
            col_num += 1;
        } else{
             if(direct == Direction::downLeft){
                row_num += 1;
                col_num -= 1;
             }
        }

        if (row_num < 0 || row_num >= ROWS || col_num < 0 || col_num >= COLS){
            return 0;
        }
        
        if (matrix[row_num][col_num] == '.'){
            return 0;
        }
        if (matrix[row_num][col_num] == color){
            break;
        }
    }

    return 1;
}

void playerInit(Player *player1, Player *player2){

    player1->name.assign("Player 1");
    player2->name.assign("Player 2");

    string color_choice;
    std::cout << "Player 1, Please Select a Color (white or black) ";
    std::getline(std::cin, color_choice);

    
    if (color_choice.compare("white") == 0){
        player1->color = 'w';
        player2->color = 'b';
    } else{
        if (color_choice.compare("black") == 0){
            player1->color = 'b';
            player2->color = 'w';
        } else{
            std::cout << "Invalid Selection. The Game Will Now Exit.\n";
            exit(1);
        }
    }

}

void boardInit(ReversiDimensions *board) {

    int userInput;
    char term;
    std::cout << "Please Enter the Size of the Board (Integer Between 5-98): ";
    std::cin >> userInput;
    if(std::cin.fail() || userInput < 5){ 
        std::cout << "Invalid Input, The Game Will Now Exit.\n";
        exit(1);
    }else{
        board->ROWS = userInput;
        board->COLUMNS = userInput;     
    }
}

void printBoard(char **game_board, int ROWS, int COLS){

    int k, l , m;
    for (k=0; k<ROWS; k++){
        if (k == ROWS - 1){
            std::cout << " " << k << "\n";
        }else if (k == 0){
            if(k<10){
                std::cout << "  " << k << " ";
            }else{
                std::cout << "  " << k;
            }
        }else{
            std::cout << " " << k << " ";
        }
    }
    for (l=0; l<ROWS; l++){
        for (m=0; m < COLS; m++){
            if (m == COLS - 1){
                std::cout << " " << game_board[l][m] << "\n";
            }else if(m == 0){

                if (l < 10){
                    std::cout << l << "  " << game_board[l][m] << " ";
                }else{
                    std::cout << l << " " << game_board[l][m] << " ";
                }

            }else{
                std::cout << " " << game_board[l][m] << " ";
            }
        }
    }
}

int getScore(char **matrix, int ROWS, int COLS, char color){

    //Based on color parameter, iterate over board and return number instances of that color on the current board. 
    int i,j, score;
    score = 0;
    for (i=0; i<ROWS; i++){
        for(j=0; j<COLS; j++){
            if (matrix[i][j] == color){
                score += 1;
            }
        }
    }
    return score;
}

int boardFull(char **matrix, int ROWS, int COLS){

    //Iterate over the board and check if any spaces are still empty (e.g. == '.'). If there
    //exists at least one empty space, return false.
    int i,j;
    for (i=0; i<ROWS; i++){
        for(j=0; j<COLS; j++){
            if (matrix[i][j] == '.'){
                return 0;
            }
        }
    }
    return 1;
    
}

int noValidMovesCheck(char **game_board, int ROWS, int COLS, Player *player1){
    int i,j;
    //player1 valid move checker variables.
    int player1_valid;
    int player1_up;
    int player1_down;
    int player1_left;
    int player1_right; 
    int player1_up_right;
    int player1_up_left;
    int player1_down_right;
    int player1_down_left; 


    for (i=0; i<ROWS; i++){
        for(j=0; j<COLS; j++){
            //check up/down and left/right positions of board for valid moves.
            player1_up = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::up);
            player1_down = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::down);
            player1_left = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::left);
            player1_right = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::right);

            //Check validity of move in diagonal directions.
            player1_up_right = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::upRight);
            player1_up_left = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::upLeft);
            player1_down_right = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::downRight);
            player1_down_left = checkValidDirection(game_board, COLS, ROWS, j, i, player1->color, Direction::downLeft);

            player1_valid = (player1_up | player1_down | player1_left | player1_right | player1_up_right | player1_up_left | player1_down_right | player1_down_left);

            if (player1_valid == 1){
                return 0;
            }
        }
        
    }

    return 1;
}

int run(ReversiDimensions *board, Player *player1, Player *player2){
 
    int i,j,l,m,k;

    int ROWS = board->ROWS;
    int COLS = board->COLUMNS;

    //Allocate Memory to Initialize Board based on dimensions given by "*board" parameter.
    char **game_board = nullptr;
    game_board = new char * [ROWS];
    for (i = 0; i < ROWS; i++) {
        game_board[i] = new char[COLS];
        for (j = 0; j < COLS; j++) {
            game_board[i][j] = '.';
        }
    }

    //Add initial 4 tiles to the board.
    game_board[ (ROWS/2)-1 ][ (COLS/2)-1 ] = 'b';
    game_board[ (ROWS/2)-1 ][ (COLS/2) ] = 'w';
    game_board[ (ROWS/2) ][ (COLS/2)-1 ] = 'w';
    game_board[ (ROWS/2) ][ (COLS/2) ] = 'b';


    //Print initial board layout. 
    for (k=0; k<ROWS; k++){
        if (k == ROWS - 1){
            std::cout << " "<< k << "\n";
        }else if (k == 0){
            if(k<10){
                std::cout << "  " << k << " ";
            }else{
                std::cout << "  " << k;
            }
        }else{
            std::cout << " " << k << " ";
        }
    }
    for (l=0; l<ROWS; l++){
        for (m=0; m < COLS; m++){
            if (m == COLS - 1){
                std::cout << " " << game_board[l][m] << "\n";
            }else if(m == 0){

                if (l < 10){
                    std::cout << l << "  " << game_board[l][m] << " ";
                }else{
                    std::cout << l << " " << game_board[l][m] << " ";
                }

            }else{
                std::cout << " " << game_board[l][m] << " ";
            }
        }
    }

    int gameOver = 0;

    //Run game until no valid moves remain or the board is full.
    while (gameOver == 0){

        //player1 valid move checker variables.
        int player1_valid;
        int player1_up;
        int player1_down;
        int player1_left;
        int player1_right; 
        int player1_up_right;
        int player1_up_left;
        int player1_down_right;
        int player1_down_left; 

        //player1 valid move checker variables.
        int player2_valid;
        int player2_up;
        int player2_down;
        int player2_left;
        int player2_right; 
        int player2_up_right;
        int player2_up_left;
        int player2_down_right;
        int player2_down_left; 

        int board_full, player1_score, player2_score, p1_valid_moves_check, p2_valid_moves_check;

        while(1){

            int counter = 0;
            int initCol;
            int initRow;

            int *possible_cols = nullptr;
            possible_cols = new int[COLS*ROWS];

            int *possible_rows = nullptr;
            possible_rows = new int[COLS*ROWS];

            p1_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player1);
            if (p1_valid_moves_check == 1){
                p2_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player2);
                if (p2_valid_moves_check == 1){
                    printBoard(game_board, ROWS, COLS);
                    player1_score = getScore(game_board, ROWS, COLS, player1->color);
                    player2_score = getScore(game_board, ROWS, COLS, player2->color);
                    std::cout << player1->name << " Score: " << player1_score;
                    std::cout << player2->name << " Score: " << player2_score;
                    delete[] possible_cols;
                    delete[] possible_rows;
                    gameOver = 1;
                    if (player1_score > player2_score){
                        std::cout << player1->name << " Wins!\n";
                        break;
                    } else{
                        std::cout << player2->name << " Wins!\n";
                        break;
                    }

                }else{
                    std::cout << "No Valid Moves for " << player1->color << "\n";
                    break;
                }
            }

            int move1r;
            int move1c;

            std::cout << player1->name << " Please Specify a ROW (as an integer) ";
            std::cin >> move1r;
            while ( !std::cin ){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Input. Try Again.\n";
                std::cout << player1->name << " Please Specify a ROW (as an integer) ";
                std::cin >> move1r;   
            }
            std::cout << player1->name << " Please Specify a COLUMN (as an integer) ";
            std::cin >> move1c;
            while ( !std::cin ){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Input. Try Again.\n";
                std::cout << player1->name << " Please Specify a COLUMN (as an integer) ";
                std::cin >> move1r;   
            }

            std::cin.clear();
            std::cin.ignore();
            //std::getline(std::cin, move1);
            
            //int row1 = move1[0] - '0';
            //int col1 = move1[2] - '0';
            int row1 = move1r;
            int col1 = move1c;
            std::cout << "Row: " << row1 << " Col: " << col1 << "\n";
            
            //Check if the position given is out of range of board to avoid seg fault. 
            if (row1 < 0 || row1 >= ROWS || col1 < 0 || col1 >= COLS){
                player1_valid = 0;
            }else{
            
                //Check if move is valid by checking left/right and up/down directions.
                player1_up = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::up, possible_rows, possible_cols, &counter);
                player1_down = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::down, possible_rows, possible_cols, &counter);
                player1_left = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::left, possible_rows, possible_cols, &counter);
                player1_right = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::right, possible_rows, possible_cols, &counter);

                //Check if move is valid by checking diagonal direction of specified position. 
                player1_up_right = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::upRight, possible_rows, possible_cols, &counter);
                player1_up_left = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::upLeft, possible_rows, possible_cols, &counter);
                player1_down_right = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::downRight, possible_rows, possible_cols, &counter);
                player1_down_left = checkDirection(game_board, COLS, ROWS, col1, row1, player1->color, Direction::downLeft, possible_rows, possible_cols, &counter);

                player1_valid = (player1_up | player1_down | player1_left | player1_right | player1_up_right | player1_up_left | player1_down_right | player1_down_left);
            }

            if (player1_valid == 0){
                std::cout << "Invalid Move\n";

            }else{

                initRow = row1;
                initCol = col1;

                int i;
                for (i=0; i<counter; i++){
                    game_board[possible_rows[i]][possible_cols[i]] = player1->color;
                }
                game_board[initRow][initCol] = player1->color;

                board_full = boardFull(game_board, ROWS, COLS);
                p1_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player1);
                p2_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player2);

                if (board_full == 1 || (p1_valid_moves_check == 1 && p2_valid_moves_check == 1)){
                    player1_score = getScore(game_board, ROWS, COLS, player1->color);
                    player2_score = getScore(game_board, ROWS, COLS, player2->color);
                    std::cout << "No Valid Moves Left For Either Player.";
                    printBoard(game_board, ROWS, COLS);
                    std::cout << player1->name << " Score: " << player1_score << "\n";
                    std::cout << player2->name << " Score: " << player2_score << "\n";
                    delete[] possible_cols;
                    delete[] possible_rows;
                    gameOver = 1;
                    if (player1_score > player2_score){
                        std::cout << player1->name << " Wins!\n";
                        break;
                    } else{
                        std::cout << player2->name <<" Wins!\n";
                        break;
                    }
                }
                printBoard(game_board, ROWS, COLS);
                break;
            }

        }
        if (gameOver == 1){
            break;
        }

        while(1){
            int counter2 = 0;
            int initRow2;
            int initCol2;

            int *possible_cols2 = nullptr;
            possible_cols2 = new int[COLS*ROWS];

            int *possible_rows2 = nullptr;
            possible_rows2 = new int[COLS*ROWS];

            p2_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player2);
            if (p2_valid_moves_check == 1){
                p1_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player1);
                if (p1_valid_moves_check == 1){
                    player1_score = getScore(game_board, ROWS, COLS, player1->color);
                    player2_score = getScore(game_board, ROWS, COLS, player2->color);
                    printBoard(game_board, ROWS, COLS);
                    std::cout << player1->name << " Score: " << player1_score;
                    std::cout << player2->name << " Score: " << player2_score;
                    delete[] possible_cols2;
                    delete[] possible_rows2;
                    gameOver = 1;
                    if (player1_score > player2_score){
                        std::cout << player1->name << " Wins!\n";
                        break;
                    } else{
                        std::cout << player2->name <<" Wins!\n";
                        break;
                    }

                }else{
                    std::cout << "No Valid Moves for " << player2->color << "\n";
                    break;
                }
            }
            int move2r;
            int move2c;

            std::cout << player2->name << " Please Specify a ROW (as an integer) ";
            std::cin >> move2r;
            while ( !std::cin ){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Input. Try Again.\n";
                std::cout << player2->name << " Please Specify a ROW (as an integer) ";
                std::cin >> move2r;   
            }
            std::cout << player2->name << " Please Specify a COLUMN (as an integer) ";
            std::cin >> move2c;
            while ( !std::cin ){
                std::cin.clear ();    // Restore input stream to working state
                std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
                std::cout << "Invalid Input. Try Again.\n";
                std::cout << player2->name << " Please Specify a COLUMN (as an integer) ";
                std::cin >> move2c;   
            }
            std::cin.clear();
            std::cin.ignore();
            //std::getline(std::cin, move2);

            //int row2 = move2[0] - '0';
            //int col2 = move2[2] - '0';
            int row2 = move2r;
            int col2 = move2c;
            std::cout << "Row: " << row2 << " Col: " << col2 << "\n";

            if (row2 < 0 || row2 >= ROWS || col2 < 0 || col2 >= COLS){
                player2_valid = 0;
            } else{

                player2_up = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::up, possible_rows2, possible_cols2, &counter2);
                player2_down = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::down, possible_rows2, possible_cols2, &counter2);
                player2_left = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::left, possible_rows2, possible_cols2, &counter2);
                player2_right = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::right, possible_rows2, possible_cols2, &counter2);

                //Check if move is valid by checking diagonal direction of specified position. 
                player2_up_right = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::upRight, possible_rows2, possible_cols2, &counter2);
                player2_up_left = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::upLeft, possible_rows2, possible_cols2, &counter2);
                player2_down_right = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::downRight, possible_rows2, possible_cols2, &counter2);
                player2_down_left = checkDirection(game_board, COLS, ROWS, col2, row2, player2->color, Direction::downLeft, possible_rows2, possible_cols2, &counter2);

                player2_valid = (player2_up | player2_down | player2_left | player2_right | player2_up_right | player2_up_left | player2_down_right | player2_down_left);
            }
            if (player2_valid == 0){
                std::cout << "Invalid Move\n";

            }else{
                initRow2 = row2;
                initCol2 = col2;

                int j;
                for (j=0; j<counter2; j++){
                    game_board[possible_rows2[j]][possible_cols2[j]] = player2->color;
                }
                game_board[initRow2][initCol2] = player2->color;

                board_full = boardFull(game_board, ROWS, COLS);
                p1_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player1);
                p2_valid_moves_check = noValidMovesCheck(game_board, ROWS, COLS, player2);
                if (board_full == 1 || (p1_valid_moves_check == 1 && p2_valid_moves_check == 1)){
                    player1_score = getScore(game_board, ROWS, COLS, player1->color);
                    player2_score = getScore(game_board, ROWS, COLS, player2->color);
                    std::cout << "No Valid Moves Left For Either Player.\n";
                    printBoard(game_board, ROWS, COLS);
                    std::cout << player1->name << " Score: " << player1_score << "\n";
                    std::cout << player2->name << " Score: " << player2_score << "\n";
                    gameOver = 1;
                    delete[] possible_cols2;
                    delete[] possible_rows2;
                    if (player1_score > player2_score){
                        std::cout << player1->name <<" Wins!" << "\n";
                        break;
                    } else{
                        std::cout << player2->name << " Wins!" << "\n";
                        break;
                    }
                }
                printBoard(game_board, ROWS, COLS);
                break;
            }
        }

    }
    int d;
    for (d = 0; d < ROWS; d++)
		delete [] game_board[i];
	delete [] game_board;


    return 0;
}

/*
int main(){

    ReversiDimensions board;
    Player first_player;
    Player second_player;
    playerInit(&first_player, &second_player);
    std::cout << "Player 1 Color: " << first_player.color << "\n";
    std::cout << "Player 2 Color: " <<  second_player.color << "\n";
    boardInit(&board);
    std::cout << "Board Dimensions: " << board.ROWS << "x" << board.COLUMNS << "\n";

    //Declare player1 and player2 based on player using black tiles.
    if (first_player.color == 'b'){
        run(&board, &first_player, &second_player);
    } else{
        run(&board, &second_player, &first_player);
    }

}
*/