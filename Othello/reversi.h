#ifndef REVERSI_H_
#define REVERSI_H_
#include <string>
using namespace std;

//enum struct to check directions
enum Direction{
    up,down,left,right,upRight,upLeft,downRight,downLeft 
};

//board dimesions initializer struct
class ReversiDimensions{
    public:
    int ROWS;
    int COLUMNS;
};

//player struct with color (black or white) attribute
class Player{

    public:
    char color;
    std::string name;

};

int checkDirection(char **matrix, int COLS, int ROWS, int col_num, int row_num, char color, Direction direct, int *possible_rows, int *possible_cols, int *counter);

int checkValidDirection(char **matrix, int COLS, int ROWS, int col_num, int row_num, char color, Direction direct);

void playerInit(Player *player1, Player *player2);

void boardInit(ReversiDimensions *board);

void printBoard(char **game_board, int ROWS, int COLS);

int getScore(char **matrix, int ROWS, int COLS, char color);

int boardFull(char **matrix, int ROWS, int COLS);

int noValidMovesCheck(char **game_board, int ROWS, int COLS, Player *player1);

int run(ReversiDimensions *board, Player *player1, Player *player2);



#endif /* REVERSI_H_ */