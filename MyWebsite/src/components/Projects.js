import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import { Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button } from "@material-ui/core";
import "../styling/Projects.css";

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 140,
    }
});

const Projects = () => {

    const img_paths = [require("../assets/keysafe_logo.png"), require("../assets/whatskraken_logo.png"), require("../assets/datatrack_logo.png")]

    const projects = {

        "KeySafe Password Manager": ["A secure password manager utilizing industry-standard security practices to provide clients with a safe place to store and access their passwords. You can trust KeySafe to maintain the integrity, accessibility, and confidentiality of user data.", img_paths[0], "projects/keysafe"],
        "Networking Web App": ["Full-stack web application featuring an integrated registration process, complete profile customization, and more. Made primarily for university students and faculty, What's Kraken is a networking platform that keeps you connected with your colleagues and professors.", img_paths[1], "projects/whatskraken"],
        "DataTrack Mobile App": ["React Native application featuring the ability to track 2-dimensional data over time. Supports Google OAuth for account management, line chart, bar graph, and pie chart plotting.", img_paths[2], "projects/datatrack"]

    }

    const classes = useStyles();

    var proj_components = []
    
    for (const [key, value] of Object.entries(projects)){

        proj_components.push(
            <Card className={classes.root} key={key} style={{margin:"0 auto", marginTop:"15px"}}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        className={classes.media}
                        image={value[1]}
                        title={key}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {key}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {value[0]}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        <NavLink exact to={value[2]}>Learn More</NavLink>
                    </Button>
                </CardActions>
            </Card>
        )
    }

    return (
    <div className="bodyContainer">
        <h1>My Projects</h1>
        <hr />
        <div id="project_cards">
            {proj_components}
        </div>
    </div>

    )
}

export default Projects;